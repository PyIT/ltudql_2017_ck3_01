USE master
GO
--IF EXISTS(select * from sys.databases where name='GymManagement1.1')
--DROP DATABASE [GymManagement1.1]
--GO 
CREATE DATABASE [GymManagement]
GO
USE [GymManagement]
GO


--Tạo TABLE TAIKHOAN
--Ràng buộc: Nhân viên phải trên 18 tuổi
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TAIKHOAN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TAIKHOAN](
	MaTK INT NOT NULL IDENTITY(1,1),
	TenDangNhap Varchar(50) not NULL UNIQUE,
	MatKhau Varchar(1000) Not NULL,
	LoaiTK BIT Not NULL, --Bit( 1 là tài khoản chủ	phòng, 0 là tài khoản nhân viên)
	GioiTinh BIT Not null,-- 1 là nam 0 là nữ
	HoTen Nvarchar(50) Not NULL,
	Email Varchar(50) Not NULL UNIQUE,
	SDT Varchar(15) Not NULL UNIQUE,
	DiaChi Nvarchar(100) Not NULL,
	LuongCB Float Not NULL,
	NgaySinh DATE NOT NULL,
	BangCap Varchar(50) NOT NULL,
	TinhTrang BIT Not null, --1 là còn làm việc, 0 là đa nghi việc.
	AnhTK VARCHAR(50)
 CONSTRAINT [PK_TAIKHOAN] PRIMARY KEY CLUSTERED 
(
	[MaTK] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

INSERT INTO dbo.TAIKHOAN
(	TenDangNhap ,
	MatKhau ,
	LoaiTK ,
    GioiTinh ,
	HoTen ,
	Email ,
	SDT ,
	DiaChi ,
	LuongCB ,
	NgaySinh ,
	BangCap ,
	TinhTrang
)
VALUES  ('admin','e10adc3949ba59abbe56e057f20f883e',1,1,'Py ADMIN','Py.ltk1998@gmail.com','0379722235','275 Le Van Viet Q9',15000000,'1998-09-14','THPT',1)

GO

INSERT INTO dbo.TAIKHOAN
(	TenDangNhap ,
	MatKhau ,
	LoaiTK ,
    GioiTinh ,
	HoTen ,
	Email ,
	SDT ,
	DiaChi ,
	LuongCB ,
	NgaySinh ,
	BangCap ,
	TinhTrang
)
VALUES  ('letan','e10adc3949ba59abbe56e057f20f883e',0,1,'Py LT1','PyLT@gmail.com','0379722342','275 Le Van Viet Q9',5000000,'1998-09-14','THPT',1)

GO

INSERT INTO dbo.TAIKHOAN
(	TenDangNhap ,
	MatKhau ,
	LoaiTK ,
    GioiTinh ,
	HoTen ,
	Email ,
	SDT ,
	DiaChi ,
	LuongCB ,
	NgaySinh ,
	BangCap ,
	TinhTrang
)
VALUES  ('letan2','e10adc3949ba59abbe56e057f20f883e',0,0,'Py LT2','PyLT2@gmail.com','03797','275 Le Van Viet Q9',5000000,'1998-09-14','THPT',0)

GO

--Tạo TABLE HLVCANHAN
--Ràng buộc: PT phải trên 18 và dưới 65 tuổi
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HLVCANHAN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[HLVCANHAN](
	MaPT INT NOT NULL IDENTITY(1,1),--Khoá chính tự động tăng
	TenPT Nvarchar(50) Not NULL,
	GioiTinhPT bit Not null, --1 là nam 0 là nữ
	EmailPT Varchar(50) Not NULL,
	SdtPT Varchar(15) Not NULL,
	DiaChiPT Nvarchar(100) Not NULL,
	BoMonPT NVARCHAR(100) NOT NULL,
	LuongCB Float Not NULL,
	NgaySinhPT DATE NOT NULL,
	BangCap Varchar(50) NOT NULL,
	TinhTrangPT bit Not NULL , --1 là còn làm việc, 0 là  đa nghi việc.
	AnhPT VARCHAR(100)

 CONSTRAINT [PK_HLVCANHAN] PRIMARY KEY CLUSTERED 
(
	[MaPT] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
insert into dbo.HLVCANHAN(TenPT, GioiTinhPT, EmailPT, SdtPT, DiaChiPT, BoMonPT, LuongCB, NgaySinhPT, BangCap, TinhTrangPT) values('Py', 1, 'PyHLV@gmail.com', '8888', 'Vincom Q9', 'Gym', 11000000, '1998-01-01', 'THPT', 1)
GO
insert into dbo.HLVCANHAN(TenPT, GioiTinhPT, EmailPT, SdtPT, DiaChiPT, BoMonPT, LuongCB, NgaySinhPT, BangCap, TinhTrangPT) values('Jack', 1, 'Jax@gmail.com', '8888', 'Vincom Q9', 'Yoga', 11000000, '1998-01-01', 'THPT', 1)
GO
insert into dbo.HLVCANHAN(TenPT, GioiTinhPT, EmailPT, SdtPT, DiaChiPT, BoMonPT, LuongCB, NgaySinhPT, BangCap, TinhTrangPT) values('Py2', 1, 'PyHLV2@gmail.com', '8888', 'Vincom Q9', 'Gym', 11000000, '1998-01-01', 'THPT', 1)
GO




--Tạo TABLE PHANCONG
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PHANCONG]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[PHANCONG](
	MaPC INT NOT NULL IDENTITY(1,1),
	MaPT Int  not null,
	MaBM INT NOT NULL

 CONSTRAINT [PK_PHANCONG] PRIMARY KEY CLUSTERED 
(
	[MaPC] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

--Tạo TABLE LICHPT
--Ràng buộc: Ngày bắt đầu phải nhỏ hơn ngày kết thúc
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LICHPT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[LICHPT](
	MaLichPT INT NOT NULL IDENTITY(1,1),
	ThangBatDauPT Date Not null default GETDATE(),
	ThangKetThucPT Date Not NULL,
	MaPT Int Not null --khóa ngoại với bảng PERSONALTRAINER

 CONSTRAINT [PK_LICHPT] PRIMARY KEY CLUSTERED 
(
	[MaLichPT] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO


--Tạo TABLE  BOMON
--Ràng buộc: Có 4 loại hình tập cơ bản: Gym, Yoga, Calisthenics, Aerobic
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BOMON]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[BOMON](
	MaBM INT NOT NULL IDENTITY(1,1),
	TenBM Varchar(50) Not NULL,
	GiaBM Float Not null

 CONSTRAINT [PK_BOMON] PRIMARY KEY CLUSTERED 
(
	[MaBM] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
insert into dbo.BOMON(TenBM, GiaBM) values('Gym',300000)
GO
insert into dbo.BOMON(TenBM, GiaBM) values('Yoga',500000)
GO


--Tạo TABLE  KHACHHANG
--Ràng buộc: Khách hàng phải trên 14 tuổi, ngày kết thúc phải nhỏ hơn ngày bắt đầu
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KHACHHANG]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[KHACHHANG](
	MaKH INT NOT NULL IDENTITY(1,1),
	HoTenKH Nvarchar(50) Not NULL,
	GioiTinhKH BIT Not null,-- 1 là nam 0 là nữ
	DiaChiKH Varchar(50) Not null,
	SdtKH Varchar(50) Not NULL,
	NgaySinhKH DATE NOT NULL,
	NgayDangKyKH Date Not NULL,
	NgayKetThucKH Date Not NULL,
	NgayKetThucHLV Date,
	AnhKH VARCHAR(100),
	GoiTap  Varchar(50),
	MaPT INT,-- Khóa ngoại đến bảng PERSONALTRAINERl
	MaHD INT -- Khóa ngoại đến bảng HOADON edit
 CONSTRAINT [PK_KHACHHANG] PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
insert into KHACHHANG(HoTenKH,GioiTinhKH,DiaChiKH,SdtKH,NgaySinhKH,NgayDangKyKH,NgayKetThucKH) values('Soonbin','1','Quan 1, TP.HCM','01234524','6/30/1999','1/9/2020','1/9/2020')
GO
insert into KHACHHANG(HoTenKH,GioiTinhKH,DiaChiKH,SdtKH,NgaySinhKH,NgayDangKyKH,NgayKetThucKH) values('Tung','1','Quan 3, TP.HCM','014524','6/30/1999','1/9/2020','1/9/2020')
GO
--Tạo TABLE  HOADON
--Ràng buộc: Hóa đơn phải có ngày kết thúc phải nhỏ hơn ngày bắt đầu
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HOADON]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[HOADON](
	MaHD INT NOT NULL IDENTITY(1,1),
	ChiTiet Nvarchar(1000) not null,
	TongTien Float not null

 CONSTRAINT [PK_HOADON] PRIMARY KEY CLUSTERED 
(
	[MaHD] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

--Tạo TABLE  CT_HOADON_STORE 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CT_HOADON_STORE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[CT_HOADON_STORE](
	MaCTHDStore INT NOT NULL IDENTITY(1,1),
	ChiTiet Nvarchar(1000) not null,
	TongTien Float Not null
 CONSTRAINT [PK_CT_HOADON_STORE] PRIMARY KEY CLUSTERED 
(
	[MaCTHDStore] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO



--Tạo TABLE  THIETBI
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[THIETBI]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[THIETBI](
	MaTB INT NOT NULL IDENTITY(1,1),
	TenTB Varchar(100) Not NULL,
	SoLuongTB Int Not null
 CONSTRAINT [PK_THIETBI] PRIMARY KEY CLUSTERED 
(
	[MaTB] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
insert into THIETBI(TenTB,SoLuongTB) values('Ta 5Kg',6)
go
insert into THIETBI(TenTB,SoLuongTB) values('Ta 25Kg',4)
go
--Tạo TABLE  LUONGNV
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LUONGNV]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[LUONGNV](
	MaLuongNV INT NOT NULL IDENTITY(1,1),
	MaNV Int Not null,-- Khóa ngoại với bảng TAIKHOAN
	LuongNV Float Not null, --Khóa ngoại với bảng TAIKHOAN
	ThangNV Date Not NULL,
	TruLuongNV Float Not NULL,
	TongLuongNV Float Not null
 CONSTRAINT [PK_LUONGNV] PRIMARY KEY CLUSTERED 
(
	[MaLuongNV] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO


--Tạo TABLE  LUONGPT
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LUONGPT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[LUONGPT](
	MaLuongPT INT NOT NULL IDENTITY(1,1),
	MaPT Int Not null,-- khóa ngoại với bảng PERSONALTRAINER
	LuongPT Float Not null, --khóa ngoại với bảng PERSONALTRAINER
	ThangPT Date Not NULL,
	ThuongPT Float Not NULL,
	TongLuongPT Float Not null
 CONSTRAINT [PK_LUONGPT] PRIMARY KEY CLUSTERED 
(
	[MaLuongPT] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

--Tạo TABLE   DOANHTHU
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DOANHTHU]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[DOANHTHU](
	MaSoDoanhThu INT NOT NULL IDENTITY(1,1),
	ThangDT Date Not NULL DEFAULT GETDATE(),
	LuongThuePT Float Not NULL,
	LuongThueNV Float Not NULL,
	TongChiTieuDT Float Not NULL,
	TongDoanhThu Float Not NULL,
	ThueDT Float Default 0.1,
	TongLaiDT Float Not null
 CONSTRAINT [PK_DOANHTHU] PRIMARY KEY CLUSTERED 
(
	[MaSoDoanhThu] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO



--Tạo TABLE  SANPHAM
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SANPHAM]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[SANPHAM](
	MaSP INT NOT NULL IDENTITY(1,1),
	TenSP NVARCHAR(100) Not null,
	GiaSP Float Not null, --Ma San Pham 
	SoLuongTon INT NOT NULL
	
 CONSTRAINT [PK_SANPHAM] PRIMARY KEY CLUSTERED 
(
	MaSP ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

insert into dbo.SANPHAM(TenSP, GiaSP, SoLuongTon) values('Best BCAAs', 600000, 20)
GO
insert into dbo.SANPHAM(TenSP, GiaSP, SoLuongTon) values('R1 Protein', 1600000, 50)

GO
--Khoá Ngoại


/****** Object:  ForeignKey [FK_PHANCONG_HLVCANHAN]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_PHANCONG_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[PHANCONG] DROP CONSTRAINT [FK_PHANCONG_HLVCANHAN]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_PHANCONG_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[PHANCONG]  WITH CHECK ADD  CONSTRAINT [FK_PHANCONG_HLVCANHAN] FOREIGN KEY([MaPT])
REFERENCES [dbo].[HLVCANHAN] ([MaPT])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_PHANCONG_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[PHANCONG] CHECK CONSTRAINT [FK_PHANCONG_HLVCANHAN]
GO
---------------------------------------------------------------------------
/****** Object:  ForeignKey [FK_PHANCONG_BOMON]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_PHANCONG_BOMON]') AND type = 'F')
ALTER TABLE [dbo].[PHANCONG] DROP CONSTRAINT [FK_PHANCONG_BOMON]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_PHANCONG_BOMON]') AND type = 'F')
ALTER TABLE [dbo].[PHANCONG]  WITH CHECK ADD  CONSTRAINT [FK_PHANCONG_BOMON] FOREIGN KEY([MaBM])
REFERENCES [dbo].[BOMON] ([MaBM])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_PHANCONG_BOMON]') AND type = 'F')
ALTER TABLE [dbo].[PHANCONG] CHECK CONSTRAINT [FK_PHANCONG_BOMON]
GO
---------------------------------------------------------------------------
/****** Object:  ForeignKey [FK_LICHPT_HLVCANHAN]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LICHPT_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[LICHPT] DROP CONSTRAINT [FK_LICHPT_HLVCANHAN]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LICHPT_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[LICHPT]  WITH CHECK ADD  CONSTRAINT [FK_LICHPT_HLVCANHAN] FOREIGN KEY([MaPT])
REFERENCES [dbo].[HLVCANHAN] ([MaPT])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LICHPT_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[LICHPT] CHECK CONSTRAINT [FK_LICHPT_HLVCANHAN]
GO
---------------------------------------------------------------------------

/****** Object:  ForeignKey [FK_KHACHHANG_HLVCANHAN]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_KHACHHANG_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[KHACHHANG] DROP CONSTRAINT [FK_KHACHHANG_HLVCANHAN]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_KHACHHANG_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[KHACHHANG]  WITH CHECK ADD  CONSTRAINT [FK_KHACHHANG_HLVCANHAN] FOREIGN KEY([MaPT])
REFERENCES [dbo].[HLVCANHAN] ([MaPT])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_KHACHHANG_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[KHACHHANG] CHECK CONSTRAINT [FK_KHACHHANG_HLVCANHAN]
GO
---------------------------------------------------------------------------


/****** Object:  ForeignKey [FK_HOADON_KHACHHANG]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_HOADON_KHACHHANG]') AND type = 'F')
ALTER TABLE [dbo].[HOADON] DROP CONSTRAINT [FK_HOADON_KHACHHANG]
GO
--IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_HOADON_KHACHHANG]') AND type = 'F')
--ALTER TABLE [dbo].[HOADON]  WITH CHECK ADD  CONSTRAINT [FK_HOADON_KHACHHANG] FOREIGN KEY([MaKH])
--REFERENCES [dbo].[KHACHHANG] ([MaKH])
--GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_HOADON_KHACHHANG]') AND type = 'F')
ALTER TABLE [dbo].[HOADON] CHECK CONSTRAINT [FK_HOADON_KHACHHANG]
GO


---------------------------------------------------------------------------
/****** Object:  ForeignKey [FK_CT_HOADON_HOADON]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_HOADON]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON] DROP CONSTRAINT [FK_CT_HOADON_HOADON]
--GO
--IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_HOADON]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON]  WITH CHECK ADD CONSTRAINT [FK_CT_HOADON_HOADON] FOREIGN KEY([MaHD])
--REFERENCES [dbo].[HOADON] ([MaHD])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_HOADON]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON] CHECK CONSTRAINT[FK_CT_HOADON_HOADON]
GO

---------------------------------------------------------------------------
--/****** Object:  ForeignKey [FK_CT_HOADON_STORE_HOADON]   ****/
--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_HOADON]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_STORE] DROP CONSTRAINT [FK_CT_HOADON_STORE_HOADON]
--GO
--IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_HOADON]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_STORE]  WITH CHECK ADD CONSTRAINT [FK_CT_HOADON_STORE_HOADON] FOREIGN KEY([MaHD])
--REFERENCES [dbo].[HOADON] ([MaHD])
--GO
--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_HOADON]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_STORE] CHECK CONSTRAINT[FK_CT_HOADON_STORE_HOADON]
--GO

---------------------------------------------------------------------------



--
--/****** Object:  ForeignKey [FK_CT_HOADON_STORE_CT_HOADON_SANPHAM]   ****/
--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_CT_HOADON_SANPHAM]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_STORE] DROP CONSTRAINT [FK_CT_HOADON_STORE_CT_HOADON_SANPHAM]
--GO
--IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_CT_HOADON_SANPHAM]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_STORE]  WITH CHECK ADD CONSTRAINT [FK_CT_HOADON_STORE_CT_HOADON_SANPHAM] FOREIGN KEY([MaCTHDSanPham])
--REFERENCES [dbo].[CT_HOADON_SANPHAM] ([MaCTHDSanPham])
--GO
--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_CT_HOADON_SANPHAM]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_STORE] CHECK CONSTRAINT[FK_CT_HOADON_STORE_CT_HOADON_SANPHAM]
--GO
/****** Object:  ForeignKey [FK_CT_HOADON_SANPHAM_CT_HOADON_STORE]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_SANPHAM_CT_HOADON_STORE]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON_SANPHAM] DROP CONSTRAINT [FK_CT_HOADON_SANPHAM_CT_HOADON_STORE]
GO
--IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_SANPHAM_CT_HOADON_STORE]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_SANPHAM]  WITH CHECK ADD CONSTRAINT [FK_CT_HOADON_SANPHAM_CT_HOADON_STORE] FOREIGN KEY([MaCTHDStore])
--REFERENCES [dbo].[CT_HOADON_STORE] ([MaCTHDStore])
--GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_SANPHAM_CT_HOADON_STORE]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON_SANPHAM] CHECK CONSTRAINT[FK_CT_HOADON_SANPHAM_CT_HOADON_STORE]
GO


---------------------------------------------------------------------------
/****** Object:  ForeignKey [FK_CT_HOADON_STORE_KHACHHANG]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_KHACHHANG]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON_STORE] DROP CONSTRAINT [FK_CT_HOADON_STORE_KHACHHANG]
--GO
--IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_KHACHHANG]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_STORE]  WITH CHECK ADD CONSTRAINT [FK_CT_HOADON_STORE_KHACHHANG] FOREIGN KEY([MaKH])
--REFERENCES [dbo].[KHACHHANG] ([MaKH])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_STORE_KHACHHANG]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON_STORE] CHECK CONSTRAINT[FK_CT_HOADON_STORE_KHACHHANG]
GO


---------------------------------------------------------------------------
/****** Object:  ForeignKey [FK_CT_HOADON_SANPHAM_SANPHAM]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_SANPHAM_SANPHAM]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON_SANPHAM] DROP CONSTRAINT [FK_CT_HOADON_SANPHAM_SANPHAM]
--GO
--IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_SANPHAM_SANPHAM]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON_SANPHAM]  WITH CHECK ADD CONSTRAINT [FK_CT_HOADON_SANPHAM_SANPHAM] FOREIGN KEY([MaSP])
--REFERENCES [dbo].[SANPHAM] ([MaSP])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_SANPHAM_SANPHAM]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON_SANPHAM] CHECK CONSTRAINT[FK_CT_HOADON_SANPHAM_SANPHAM]
GO



---------------------------------------------------------------------------
/****** Object:  ForeignKey [FK_CT_HOADON_HLVCANHAN]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON] DROP CONSTRAINT [FK_CT_HOADON_HLVCANHAN]
--GO
--IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_HLVCANHAN]') AND type = 'F')
--ALTER TABLE [dbo].[CT_HOADON]  WITH CHECK ADD  CONSTRAINT [FK_CT_HOADON_HLVCANHAN] FOREIGN KEY([MaPT])
--REFERENCES [dbo].[HLVCANHAN] ([MaPT])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_CT_HOADON_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[CT_HOADON] CHECK CONSTRAINT [FK_CT_HOADON_HLVCANHAN]
GO

---------------------------------------------------------------------------
/****** Object:  ForeignKey [FK_LUONGNV_TAIKHOAN]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LUONGNV_TAIKHOAN]') AND type = 'F')
ALTER TABLE [dbo].[LUONGNV] DROP CONSTRAINT [FK_LUONGNV_TAIKHOAN]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LUONGNV_TAIKHOAN]') AND type = 'F')
ALTER TABLE [dbo].[LUONGNV]  WITH CHECK ADD  CONSTRAINT [FK_LUONGNV_TAIKHOAN] FOREIGN KEY([MaNV])
REFERENCES [dbo].[TAIKHOAN] ([MaTK])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LUONGNV_TAIKHOAN]') AND type = 'F')
ALTER TABLE [dbo].[LUONGNV] CHECK CONSTRAINT [FK_LUONGNV_TAIKHOAN]
GO
---------------------------------------------------------------------------

/****** Object:  ForeignKey [FK_LUONGPT_HLVCANHAN]   ****/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LUONGPT_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[LUONGPT] DROP  CONSTRAINT [FK_LUONGPT_HLVCANHAN]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LUONGPT_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[LUONGPT]  WITH CHECK ADD  CONSTRAINT [FK_LUONGPT_HLVCANHAN] FOREIGN KEY([MaPT])
REFERENCES [dbo].[HLVCANHAN] ([MaPT])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_LUONGPT_HLVCANHAN]') AND type = 'F')
ALTER TABLE [dbo].[LUONGPT] CHECK CONSTRAINT [FK_LUONGPT_HLVCANHAN]
GO
---------------------------------------------------------------------------

---------------------------------TRIGGER-----------------------------------
--Trigger TABLE TAIKHOAN
--Ràng buộc: Nhân viên phải trên 18 tuổi
--IF OBJECT_ID('UTG_TAIKHOAN','TR') IS NOT NULL
--DROP TRIGGER UTG_TAIKHOAN
--go
--CREATE TRIGGER UTG_TAIKHOAN ON dbo.TAIKHOAN
--FOR INSERT
--AS
--BEGIN
--	DECLARE @now DATE = GETDATE()
--	DECLARE @birth DATE = (SELECT Inserted.NgaySinh FROM Inserted)
--	DECLARE @YO  INT = DATEDIFF(YEAR,@birth,@now)
--	IF @YO < 18
--	BEGIN
--		RAISERROR (N'Tuổi nhân viên phải trên 18 tuổi',16,1)
--		ROLLBACK TRANSACTION
--	END
--END

--Trigger TABLE HLVCANHAN
--Ràng buộc: PT phải trên 18 và dưới 65 tuổi
--IF OBJECT_ID('UTG_HLVCANHAN','TR') IS NOT NULL
--DROP TRIGGER UTG_HLVCANHAN
--go

--CREATE TRIGGER UTG_HLVCANHAN ON dbo.HLVCANHAN
--FOR INSERT
--AS
--BEGIN
--	DECLARE @now DATE = GETDATE()
--	DECLARE @birth DATE = (SELECT Inserted.NgaySinhPT FROM Inserted)
--	DECLARE @YO  INT = DATEDIFF(YEAR,@birth,@now)
--	IF @YO < 18 AND @YO > 65
--	BEGIN
--		RAISERROR (N'Tuổi Huấn Luyện Viên phải trên 18 tuổi và dưới 65 tuổi',16,1)
--		ROLLBACK TRANSACTION
--	END
--END
--go

--Trigger TABLE LICHPT
--Ràng buộc: Ngày bắt đầu phải nhỏ hơn ngày kết thúc
--IF OBJECT_ID('UTG_LICHPT','TR') IS NOT NULL
--DROP TRIGGER UTG_LICHPT
--go

--CREATE TRIGGER UTG_LICHPT ON dbo.LICHPT
--FOR INSERT
--AS
--BEGIN
--	DECLARE @now DATE = GETDATE()
--	DECLARE @mStart DATE = (SELECT Inserted.ThangBatDauPT FROM Inserted)
--	DECLARE @mEnd DATE = (SELECT Inserted.ThangKetThucPT FROM Inserted)

	--DECLARE @m  INT = DATEDIFF(YEAR,@birth,@now)

--	IF @mStart > @mEnd
--	BEGIN
--		RAISERROR (N'Tháng bắt đầu phải nhỏ hơn tháng kết thúc',16,1)
--		ROLLBACK TRANSACTION
--	END
--END
--go