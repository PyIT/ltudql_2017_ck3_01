﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class DataProvider
    {
        private static DataProvider instance;
        public static DataProvider Instance
        {
            get { if (instance == null) instance = new DataProvider(); return DataProvider.instance; }
            private set { DataProvider.instance = value; }
        }
        private DataProvider() {}

        string connectionString = @"Data Source=PY\SQLEXPRESS;Initial Catalog=GymManagement;Integrated Security=True";

        public DataTable ExcuteQuery(string query, object[] parameter = null)
        {
            DataTable data = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    
                    SqlCommand cmd = new SqlCommand(query, con);

                    if (parameter != null)
                    {
                        string[] listParameter = query.Split(' ');
                        int i = 0;
                        foreach (string item in listParameter)
                        {
                            if (item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                i++;
                            }
                        }
                    }

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(data);

                    //SqlDataReader data = cmd.ExecuteReader();
                    //while (data.Read())
                    //{
                    //    string strPass = data.GetString(0);

                    //    return 1;
                    //}
                    con.Close();
                    return data;
                }
            }
            catch
            {
                MessageBox.Show("Lỗi kết nối !!!", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return data;
            }
        }

        public int ExcuteNonQuery(string query, object[] parameter = null)
        {
            int data = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    SqlCommand cmd = new SqlCommand(query, con);

                    if (parameter != null)
                    {
                        string[] listParameter = query.Split(' ');
                        int i = 0;
                        foreach (string item in listParameter)
                        {
                            if (item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                i++;
                            }
                        }
                    }

                    data = cmd.ExecuteNonQuery();

                    con.Close();
                    return data;
                }
            }
            catch
            {
                MessageBox.Show("Lỗi kết nối !!!", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return data;
            }
        }

        public object ExcuteScalar(string query, object[] parameter = null)
        {
            object data = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    SqlCommand cmd = new SqlCommand(query, con);

                    if (parameter != null)
                    {
                        string[] listParameter = query.Split(' ');
                        int i = 0;
                        foreach (string item in listParameter)
                        {
                            if (item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                i++;
                            }
                        }
                    }

                    data = cmd.ExecuteScalar();

                    con.Close();
                    return data;
                }
            }
            catch
            {
                MessageBox.Show("Lỗi kết nối !!!", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return data;
            }
        }
    }
}
