﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class BoMonDAO
    {
        private static BoMonDAO instance;
        public static BoMonDAO Instance
        {
            get { if (instance == null) instance = new BoMonDAO(); return BoMonDAO.instance; }
            private set { BoMonDAO.instance = value; }
        }
        private BoMonDAO() { }
        public DataTable LayDSBoMon()
        {
            try
            {
                string query = "select TenBM from dbo.BoMon";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public DataTable LayGiaBM(string name)
        {
            try
            {
                string query = "select GiaBM from BOMON where TenBM = '" + name + "'";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

    }
}
