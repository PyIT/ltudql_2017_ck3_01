﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class HLVCaNhanDAO
    {

        private static HLVCaNhanDAO instance;
        public static HLVCaNhanDAO Instance
        {
            get { if (instance == null) instance = new HLVCaNhanDAO(); return HLVCaNhanDAO.instance; }
            private set { HLVCaNhanDAO.instance = value; }
        }
        private HLVCaNhanDAO() { }


        public DataTable LayIdHLV(string name)
        {
            try
            {
                string query = "select MaPT from dbo.HLVCANHAN where TenPT = '" + name + "'";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public DataTable LayTenHLV(int id)
        {
            try
            {
                string query = "select TenPT from dbo.HLVCANHAN where MaPT = " + id;
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public DataTable LayThongTinHLV()
        {
            try
            {
                string query = "select MaPT as [Mã HLV], TenPT as [Tên HLV], GioiTinhPT as [Giới tính], EmailPT as [Email], SdtPT as [Số điện thoại], DiaChiPT as [Địa chỉ], BoMonPT as [Bộ môn], LuongCB as [Lương], BangCap as [Bằng cấp], TinhTrangPT as [Tình trạng] from dbo.HLVCANHAN";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public DataTable LayHLVTuSDT(string sdt)
        {
            try
            {
                string query = "select MaPT as [Mã HLV], TenPT as [Tên HLV], GioiTinhPT as [Giới tính], EmailPT as [Email], SdtPT as [Số điện thoại], DiaChiPT as [Địa chỉ], BoMonPT as [Bộ môn], LuongCB as [Lương], BangCap as [Bằng cấp], TinhTrangPT as [Tình trạng] from dbo.HLVCANHAN where SdtPT = '"+ sdt +"'";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public bool ChinhSuaThongTinHLV(string MaHLV, string fullName, int sex, string luong, string bangCap, string address, string email, string sdt, string boMon, int TT)
        {

            try
            {
                string query = "UPDATE dbo.HLVCANHAN SET TenPT = '"+ fullName +"', GioiTinhPT = "+ sex +", EmailPT = '"+ email +"', SdtPT = '"+ sdt +"', DiaChiPT ='"+ address +"', BoMonPT = '"+ boMon +"', LuongCB = '"+ luong +"', BangCap = '"+ bangCap +"', TinhTrangPT = "+ TT +" WHERE MaPT = '"+ MaHLV +"'";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(query);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool ThemHLV(string fullName, int sex, string luong, string bangCap, string address, string email, string sdt, string boMon, string NgaySinh)
        {

            try
            {
                string query = "insert into dbo.HLVCANHAN(TenPT, GioiTinhPT, EmailPT, SdtPT, DiaChiPT, BoMonPT, LuongCB, NgaySinhPT, BangCap, TinhTrangPT) values('"+ fullName +"', "+ sex +", '"+ email +"', '"+ sdt +"', '"+ address +"', '"+ boMon +"', "+ luong +", '"+ NgaySinh +"', '"+ bangCap +"', 1)";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(query);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
