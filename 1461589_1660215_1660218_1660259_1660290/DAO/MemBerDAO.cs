﻿using _1461589_1660215_1660218_1660259_1660290.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class MemBerDAO
    {
        private static MemBerDAO instance;
        public static MemBerDAO Instance
        {
            get { if (instance == null) instance = new MemBerDAO(); return MemBerDAO.instance; }
            private set { MemBerDAO.instance = value; }
        }
        public DataTable LoadDanhSachThanhVien()
        {
            try
            {
                string sql = "select MaKH as [ID], HoTenKH as [Họ Tên],GioiTinhKH as[Giới Tính],DiaChiKH as [Dịa Chỉ],SdtKH as [SĐT],NgaySinhKH as [Ngày Sinh],NgayDangKyKH as [Ngày Đăng Kí],NgayKetThucKH as [Ngày Kết Thúc],GoiTap as [Gói Tập],MaPT as [Mã PT], NgayKetThucHLV as [Ngày kết thúc HLV] from KHACHHANG";
                DataTable data = DataProvider.Instance.ExcuteQuery(sql);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public DataTable LayDanhSachTaiKhoanTuSDT(string sdt)
        {
            try
            {
               DataTable data = DataProvider.Instance.ExcuteQuery("select MaKH as [ID], HoTenKH as [Họ Tên],GioiTinhKH as[Giới Tính],DiaChiKH as [Dịa Chỉ],SdtKH as [SĐT],NgaySinhKH as [Ngày Sinh],NgayDangKyKH as [Ngày Đăng Kí],NgayKetThucKH as [Ngày Kết Thúc],GoiTap as [Gói Tập],MaPT as [Mã PT], NgayKetThucHLV as [Ngày kết thúc HLV] from KHACHHANG where SdtKH = '" + sdt + "'");
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public bool SuaThongTinThanhVien(string HoTenKH, int GioiTinhKH, string DiaChiKH, string SdtKH,string ID)
        {

            try
            {
                string Add = "update KHACHHANG set KHACHHANG.HoTenKH='" + HoTenKH + "',KHACHHANG.GioiTinhKH='" + GioiTinhKH + "',KHACHHANG.DiaChiKH='" + DiaChiKH + "',KHACHHANG.SdtKH='" + SdtKH + "' where KHACHHANG.MaKH='" + ID + "' ";
                int r= DAO.DataProvider.Instance.ExcuteNonQuery(Add);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
