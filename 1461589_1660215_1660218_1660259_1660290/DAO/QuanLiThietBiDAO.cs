﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class QuanLiThietBiDAO
    {
        private static QuanLiThietBiDAO instance;
        public static QuanLiThietBiDAO Instance
        {
            get { if (instance == null) instance = new QuanLiThietBiDAO(); return QuanLiThietBiDAO.instance; }
            private set { QuanLiThietBiDAO.instance = value; }
        }
        public DataTable LoadDanhSachThietBi()
        {
            try
            {
                string sql = "select MaTB as [ID], TenTB as [Tên Thiết Bị],SoLuongTB as[Số Lượng] from THIETBI";
                DataTable data = DataProvider.Instance.ExcuteQuery(sql);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public bool ThemThietBi(string TenThietBi, int SoLuong)
        {

            try
            {
                string Add = "insert into THIETBI(TenTB,SoLuongTB) values('" + TenThietBi + "','" + SoLuong + "')";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Add);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool SuaThongTinThietBi(string TenThietBi, int SoLuong, string ID)
        {

            try
            {
                string Add = "update THIETBI set THIETBI.TenTB='" + TenThietBi + "',THIETBI.SoLuongTB='" + SoLuong + "' where THIETBI.MaTB='" + ID + "' ";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Add);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool XoaThietBi(string ID)
        {

            try
            {
                string Xoa = "Delete THIETBI where THIETBI.MaTB='" + ID + "' ";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Xoa);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}

