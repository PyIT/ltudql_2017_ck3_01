﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class SanPhamDAO
    {
        private static SanPhamDAO instance;
        public static SanPhamDAO Instance
        {
            get { if (instance == null) instance = new SanPhamDAO(); return SanPhamDAO.instance; }
            private set { SanPhamDAO.instance = value; }
        }
        private SanPhamDAO() { }
        public DataTable LayDSSP()
        {
            try
            {
                string query = "select MaSP as [Mã sản phẩm], TenSP as [Tên sản phẩm], GiaSP as [Giá sản phẩm], SoLuongTon as [Số lượng] from dbo.SANPHAM";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public bool ThemSP(string name, string price, string count)
        {

            try
            {
                string query = "insert into dbo.SANPHAM(TenSP, GiaSP, SoLuongTon) values('"+ name +"', "+ price +", "+ count +")";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(query);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool XoaSP(string id)
        {

            try
            {
                string query = "delete from dbo.SanPham where MaSP = " + id;
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(query);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool SuaSP(string id, string name, string price, string count)
        {

            try
            {
                string query = "update dbo.SANPHAM set TenSP = '"+ name +"', GiaSP = "+ price +", SoLuongTon = "+ count +" where MaSP = " + id;
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(query);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public DataTable LayDSTenSP()
        {
            try
            {
                string query = "select TenSP from dbo.SanPham";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public DataTable LayGiaSP(string name)
        {
            try
            {
                string query = "select GiaSP from dbo.SanPham where TenSP = '" + name + "'";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
    }
}
