﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class GiaHanGoiTapDAO
    {
        private static GiaHanGoiTapDAO instance;
        public static GiaHanGoiTapDAO Instance
        {
            get { if (instance == null) instance = new GiaHanGoiTapDAO(); return GiaHanGoiTapDAO.instance; }
            private set { GiaHanGoiTapDAO.instance = value; }
        }
        public DataTable LayDanhSachPT(string bm)
        {
            try
            {
                string sql = "select MaPT,TenPT From HLVCANHAN where BoMonPT ='" + bm + "'";
                DataTable data = DataProvider.Instance.ExcuteQuery(sql);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
       
        public bool ThemGoiTap(int MaPT,string ID, string gtap, string NgayKT, string NgayKTHLV)
        {

            try
            {
                string Add = "update KHACHHANG set KHACHHANG.MaPT=" + MaPT + ", KHACHHANG.GoiTap='"+ gtap +"', KHACHHANG.NgayKetThucKH = '"+ NgayKT + "', KHACHHANG.NgayKetThucHLV = '"+ NgayKTHLV +"' where KHACHHANG.MaKH='" + ID + "' ";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Add);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool ThemHoaDon(string CT, string Tien)
        {

            try
            {
                string query = "insert into HOADON(ChiTiet, TongTien) values('" + CT + "'," + Tien + ")";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(query);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
