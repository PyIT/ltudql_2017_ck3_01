﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _1461589_1660215_1660218_1660259_1660290.DTO;
namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class QuanLiGoiTapDAO
    {
        private static QuanLiGoiTapDAO instance;
        public static QuanLiGoiTapDAO Instance
        {
            get { if (instance == null) instance = new QuanLiGoiTapDAO(); return QuanLiGoiTapDAO.instance; }
            private set { QuanLiGoiTapDAO.instance = value; }
        }
        public DataTable LoadDanhSachGoiTap()
        {
            try
            {
                string sql = "select MaBM as [ID], TenBM as [Tên Bộ Môn],GiaBM as[Giá] from BOMON";
                DataTable data = DataProvider.Instance.ExcuteQuery(sql);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public bool ThemGoiTap(string TenGoiTap, float GiaTien)
        {

            try
            {
                string Add = "insert into BOMON(TenBM,GiaBM) values('" + TenGoiTap + "','" + GiaTien + "')";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Add);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool SuaThongTinGoiTap(string TenGoiTap, float GiaTien,string ID)
        {

            try
            {
                string Add = "update BOMON set BOMON.TenBM='" + TenGoiTap + "',BOMON.GiaBM='" + GiaTien + "' where BOMON.MaBM='" + ID + "' ";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Add);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool XoaGoiTap(string ID)
        {

            try
            {
                string Xoa = "Delete BOMON where BOMON.MaBM='" + ID + "' ";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Xoa);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
