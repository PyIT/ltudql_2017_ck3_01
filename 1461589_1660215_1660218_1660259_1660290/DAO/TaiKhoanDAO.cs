﻿using _1461589_1660215_1660218_1660259_1660290.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.DAO
{
    class TaiKhoanDAO
    {
        private static TaiKhoanDAO instance;
        public static TaiKhoanDAO Instance
        {
            get { if (instance == null) instance = new TaiKhoanDAO(); return TaiKhoanDAO.instance; }
            private set { TaiKhoanDAO.instance = value; }
        }
        private TaiKhoanDAO() { }

        public  bool Login(string userName, string passWord)
        {
            try
            {
                /*===== HASH PASSWORD =====*/
                //Tạo MD5 
                MD5 mh = MD5.Create();
                //Chuyển kiểu chuổi thành kiểu byte
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(passWord);
                //mã hóa chuỗi đã chuyển
                byte[] hash = mh.ComputeHash(inputBytes);
                //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
                StringBuilder sbPasswordHash = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sbPasswordHash.Append(hash[i].ToString("x2"));
                }


                string query = "select * from dbo.TAIKHOAN  where TenDangNhap = N'" + userName + "' AND Matkhau = N'" + sbPasswordHash.ToString() + "'";
                DataTable result = DataProvider.Instance.ExcuteQuery(query);
                return result.Rows.Count > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public TaiKhoanDTO GetAccountByUserName(string userName)
        {
            try
            {
                DataTable data = DataProvider.Instance.ExcuteQuery("select * from dbo.TAIKHOAN  where TenDangNhap = '" + userName + "'");

                foreach (DataRow item in data.Rows)
                {
                    return new TaiKhoanDTO(item);
                }
                return null;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }


        public bool EditInformationAccount(string userName, string fullName, int sex, string birthday, string address, string email, string sdt)
        {
            try
            {
                string query = "UPDATE dbo.TAIKHOAN SET GioiTinh = "+ sex +", HoTen = '"+ fullName +"', Email = '"+ email +"', SDT = '"+ sdt +"', DiaChi = '"+ address +"', NgaySinh = '"+ birthday +"' WHERE TenDangNhap = '"+ userName +"'";
                int r = DataProvider.Instance.ExcuteNonQuery(query);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool ChangePasswordAccount(string userName, string newPassword)
        {
            try
            {
                string query = "UPDATE dbo.TAIKHOAN SET MatKhau = '"+ newPassword +"' WHERE TenDangNhap = '" + userName + "'";
                int r = DataProvider.Instance.ExcuteNonQuery(query);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public DataTable LayDSLeTan()
        {
            try
            {
                string query = "select TenDangNhap as [Tên đăng nhập], MatKhau as [Mật khẩu], HoTen as [Họ tên], GioiTinh as [Giới tính], BangCap as [Bằng cấp], SDT as [Số điện thoại], Email as [Email], DiaChi as [Địa chỉ], LuongCB as [Lương], TinhTrang as [Tình trạng] from dbo.TAIKHOAN where LoaiTK = 0";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);

                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public DataTable LayDSLTTuSDT(string sdt)
        {
            try
            {
                string query = "select TenDangNhap as [Tên đăng nhập], MatKhau as [Mật khẩu], HoTen as [Họ tên], GioiTinh as [Giới tính], BangCap as [Bằng cấp], SDT as [Số điện thoại], Email as [Email], DiaChi as [Địa chỉ], LuongCB as [Lương], TinhTrang as [Tình trạng] from dbo.TAIKHOAN where LoaiTK = 0 and SDT = '" + sdt +"'";
                DataTable data = DataProvider.Instance.ExcuteQuery(query);
                return data;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        public bool SuaThongTinLT(string userName, string fullName, int sex, string luong, string bangCap, string address, string email, string sdt, int TT)
        {

            try
            {
                string Add = "UPDATE dbo.TAIKHOAN SET GioiTinh = " + sex + ", HoTen = '" + fullName + "', Email = '" + email + "', SDT = '" + sdt + "', DiaChi = '" + address + "', LuongCB = '" + luong + "', BangCap = '" + bangCap + "', TinhTrang = '" + TT + "' WHERE TenDangNhap = '" + userName + "'";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Add);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool ThemLT(string userName, string fullName, int sex, string luong, string bangCap, string address, string email, string sdt, string Pass, string birthDay)
        {

            try
            {
                string Add = "INSERT INTO dbo.TAIKHOAN (TenDangNhap, MatKhau, LoaiTK, GioiTinh, HoTen, Email, SDT, DiaChi, LuongCB, NgaySinh, BangCap, TinhTrang) VALUES  ('"+ userName +"','"+ Pass +"',0," + sex + ",'"+ fullName +"','"+ email +"','"+ sdt +"','"+ address +"',"+ luong +",'"+ birthDay +"','"+ bangCap +"',1)";
                int r = DAO.DataProvider.Instance.ExcuteNonQuery(Add);
                return r > 0;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
