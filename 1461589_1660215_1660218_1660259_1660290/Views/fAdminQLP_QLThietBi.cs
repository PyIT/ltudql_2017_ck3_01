﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fAdminQLP_QLThietBi : Form
    {
        public fAdminQLP_QLThietBi()
        {
            InitializeComponent();
        }
        private void dataGridViewDSThietBi_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int idex = dataGridViewDSThietBi.CurrentRow.Index;
                txtID.Text = dataGridViewDSThietBi.Rows[idex].Cells[0].Value.ToString();
                txtTenThietBi.Text = dataGridViewDSThietBi.Rows[idex].Cells[1].Value.ToString();
                txtSoLuong.Text = dataGridViewDSThietBi.Rows[idex].Cells[2].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Không thể click chọn ở đây!", "Ui, có gì đó không đúng!", MessageBoxButtons.OK);
            }
        }

        private void fAdminQLP_QLThietBi_Load(object sender, EventArgs e)
        {
            dataGridViewDSThietBi.DataSource = DAO.QuanLiThietBiDAO.Instance.LoadDanhSachThietBi();
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtID.Text == "")
            {
                if (txtTenThietBi.Text != "" && txtSoLuong.Text != "")
                {
                    if (IsNumber(txtSoLuong.Text))
                    {
                        int GiaTien = int.Parse(txtSoLuong.Text);
                        if (DAO.QuanLiThietBiDAO.Instance.ThemThietBi(txtTenThietBi.Text, GiaTien))
                        {
                            MessageBox.Show("Thêm Thiết Bị thành công !", "Thành công", MessageBoxButtons.OK);
                            dataGridViewDSThietBi.DataSource = DAO.QuanLiThietBiDAO.Instance.LoadDanhSachThietBi();
                            btnThem.Enabled = true;
                            txtTenThietBi.Clear();
                            txtSoLuong.Clear();
                            txtID.Clear();

                        }
                        else MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    else
                    {
                        MessageBox.Show("Kiểm Tra Lại Số Lượng!");
                    }
                }
                else
                {
                    MessageBox.Show("Vui Lòng Kiểm Tra Lại Thông Tin");
                }
            }
            else
                MessageBox.Show("Nhấn vào trường dữ liệu trống để xóa mã trước khi thêm");
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (DAO.QuanLiThietBiDAO.Instance.XoaThietBi(txtID.Text))
            {
                if (txtSoLuong.Text != "" && txtTenThietBi.Text != "")
                {
                    MessageBox.Show("Xóa Thành Công!");
                    dataGridViewDSThietBi.DataSource = DAO.QuanLiThietBiDAO.Instance.LoadDanhSachThietBi();
                    txtTenThietBi.Clear();
                    txtSoLuong.Clear();
                    txtID.Clear();
                }
                else
                    MessageBox.Show("Chọn Thiết Bị Xóa");
            }
            else
                MessageBox.Show("Không Thể Xóa");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (txtID.Text != "")
            {
                if (txtTenThietBi.Text != "" && txtSoLuong.Text != "")
                {
                    if (IsNumber(txtSoLuong.Text))
                    {
                        int GiaTien = int.Parse(txtSoLuong.Text);
                        if (DAO.QuanLiThietBiDAO.Instance.SuaThongTinThietBi(txtTenThietBi.Text, GiaTien, txtID.Text))
                        {
                            MessageBox.Show("Sửa Thành Công!");
                            dataGridViewDSThietBi.DataSource = DAO.QuanLiThietBiDAO.Instance.LoadDanhSachThietBi();
                            txtTenThietBi.Clear();
                            txtSoLuong.Clear();
                            txtID.Clear();

                        }
                    }
                    else
                        MessageBox.Show("Kiểm Tra Số Lượng");

                }
                else
                    MessageBox.Show("Bạn Cần Nhập Đầy Đủ Thông Tin");
            }
            else MessageBox.Show("ID Không Hợp Lệ");
        }

    }
}
