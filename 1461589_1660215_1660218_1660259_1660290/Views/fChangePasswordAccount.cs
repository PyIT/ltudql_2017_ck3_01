﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using _1461589_1660215_1660218_1660259_1660290.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fChangePasswordAccount : Form
    {
        private TaiKhoanDTO login;
        internal TaiKhoanDTO Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
            }
        }
        internal fChangePasswordAccount(TaiKhoanDTO acc)
        {
            InitializeComponent();
            this.Login = acc;
        }

        private void fChangePasswordAccount_Load(object sender, EventArgs e)
        {
            try
            {
                this.Login = TaiKhoanDAO.Instance.GetAccountByUserName(this.Login.TenDangNhap);
                txtUserName.Text = this.Login.TenDangNhap;
                
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnThayDoi_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUserName.Text != "" && txtOldPW.Text != "" && txtNewPW.Text != "" && txtReNewPW.Text != "") {
                    /*===== HASH PASSWORD =====*/
                    //Tạo MD5 
                    MD5 mh = MD5.Create();
                    //Chuyển kiểu chuổi thành kiểu byte
                    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(txtOldPW.Text);
                    //mã hóa chuỗi đã chuyển
                    byte[] hash = mh.ComputeHash(inputBytes);
                    //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
                    StringBuilder sbPasswordHash = new StringBuilder();
                    for (int i = 0; i < hash.Length; i++)
                    {
                        sbPasswordHash.Append(hash[i].ToString("x2"));
                    }

                    if (this.Login.MatKhau == sbPasswordHash.ToString())
                    {
                        if (txtNewPW.Text == txtReNewPW.Text && txtNewPW.Text != "")
                        {

                            /*===== HASH PASSWORD =====*/
                            //Tạo MD5 
                            MD5 mh2 = MD5.Create();
                            //Chuyển kiểu chuổi thành kiểu byte
                            byte[] inputBytes2 = System.Text.Encoding.ASCII.GetBytes(txtNewPW.Text);
                            //mã hóa chuỗi đã chuyển
                            byte[] hash2 = mh2.ComputeHash(inputBytes2);
                            //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
                            StringBuilder sbPasswordHash2 = new StringBuilder();
                            for (int i = 0; i < hash2.Length; i++)
                            {
                                sbPasswordHash2.Append(hash2[i].ToString("x2"));
                            }

                            if (TaiKhoanDAO.Instance.ChangePasswordAccount(this.Login.TenDangNhap, sbPasswordHash2.ToString()))
                            {
                                MessageBox.Show("Thay đổi mật khẩu thành công !", "Thành công", MessageBoxButtons.OK);
                                this.Close();
                            }
                            else MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        }
                        else
                        {
                            MessageBox.Show("Mật khẩu mới không trùng khớp !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Mật khẩu cũ không chính xác !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Cần nhập đầy đủ thông tin", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
