﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using _1461589_1660215_1660218_1660259_1660290.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fChangeInformationAccount : Form
    {
        private TaiKhoanDTO login;
        internal TaiKhoanDTO Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
            }
        }

        internal fChangeInformationAccount(TaiKhoanDTO acc)
        {
            InitializeComponent();
            this.Login = acc;
        }

        private void fChangeInformationAccount_Load(object sender, EventArgs e)
        {
            try
            {
                this.Login = TaiKhoanDAO.Instance.GetAccountByUserName(this.Login.TenDangNhap);

                txtUserName.Text = this.Login.TenDangNhap;
                txtFullName.Text = this.Login.HoTen;

                //Load NgaySinh vao Control
                string Birthday = this.Login.NgaySinh;
                // Get year, month, day
                string[] resultGetDate = Birthday.Split(new char[] { '/' });
                int month = Convert.ToInt32(resultGetDate[0]);
                int day = Convert.ToInt32(resultGetDate[1]);
                string[] rsYear = resultGetDate[2].Split(new char[] { ' ' });
                int year = Convert.ToInt32(rsYear[0]);
                // Set NgaySinh vao Control
                dateTimePickerBirthDay.Value = new DateTime(year, month, day);

                // Set value and selected Sex in Control
                cbSex.Items.Clear();
                cbSex.Items.Add("Nữ");
                cbSex.Items.Add("Nam");
                if (this.Login.GioiTinh)
                    cbSex.SelectedIndex = 1;
                else cbSex.SelectedIndex = 0;

                txtAddress.Text = this.Login.DiaChi;
                txtEmail.Text = this.Login.Email;

                txtSDT.Text = this.Login.SDT;
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnThayDoi_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text != "" && txtFullName.Text != "" && cbSex.Text != "" && txtAddress.Text != "" && txtEmail.Text != "" && txtSDT.Text != "") {
                if (IsNumber(txtSDT.Text)) {
                    // Get year, month, day
                    string[] resultGetDate = dateTimePickerBirthDay.Value.ToString().Split(new char[] { '/' });
                    int month = Convert.ToInt32(resultGetDate[0]);
                    int day = Convert.ToInt32(resultGetDate[1]);
                    string[] rsYear = resultGetDate[2].Split(new char[] { ' ' });
                    int year = Convert.ToInt32(rsYear[0]);
                    // Get DATE
                    string birthday = year.ToString() + '-' + month.ToString() + '-' + day.ToString();

                    // Get sex
                    int sex = 0;
                    if (cbSex.Text == "Nam")
                        sex = 1;

                    if (TaiKhoanDAO.Instance.EditInformationAccount(txtUserName.Text, txtFullName.Text, sex, birthday, txtAddress.Text, txtEmail.Text, txtSDT.Text))
                    {
                        MessageBox.Show("Lưu chỉnh sửa thành công !", "Thành công", MessageBoxButtons.OK);
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Số điện thoại chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Cần nhập đầy đủ thông tin", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
