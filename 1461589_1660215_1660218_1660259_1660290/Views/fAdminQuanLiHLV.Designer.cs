﻿namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    partial class fAdminQuanLiHLV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnThemHLV = new System.Windows.Forms.Button();
            this.btnLuu = new System.Windows.Forms.Button();
            this.txtSearchSDT = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblSearchMember = new System.Windows.Forms.Label();
            this.grbMemberList = new System.Windows.Forms.GroupBox();
            this.dataGridViewDSHLV = new System.Windows.Forms.DataGridView();
            this.lblForm = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaHLV = new System.Windows.Forms.TextBox();
            this.cbTT = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBangCap = new System.Windows.Forms.TextBox();
            this.cbGT = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbBoMon = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grbPhoto = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnLuuAnh = new System.Windows.Forms.Button();
            this.btnChup = new System.Windows.Forms.Button();
            this.txtLuong = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.grbMemberList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDSHLV)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.grbPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnThemHLV
            // 
            this.btnThemHLV.Location = new System.Drawing.Point(568, 474);
            this.btnThemHLV.Name = "btnThemHLV";
            this.btnThemHLV.Size = new System.Drawing.Size(159, 60);
            this.btnThemHLV.TabIndex = 36;
            this.btnThemHLV.Text = "Thêm HLV";
            this.btnThemHLV.UseVisualStyleBackColor = true;
            this.btnThemHLV.Click += new System.EventHandler(this.btnThemHLV_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(354, 474);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(159, 60);
            this.btnLuu.TabIndex = 35;
            this.btnLuu.Text = "Lưu thay đổi";
            this.btnLuu.UseVisualStyleBackColor = true;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // txtSearchSDT
            // 
            this.txtSearchSDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchSDT.Location = new System.Drawing.Point(317, 51);
            this.txtSearchSDT.Name = "txtSearchSDT";
            this.txtSearchSDT.Size = new System.Drawing.Size(396, 26);
            this.txtSearchSDT.TabIndex = 33;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(733, 46);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(103, 37);
            this.btnSearch.TabIndex = 32;
            this.btnSearch.Text = "Tra cứu";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblSearchMember
            // 
            this.lblSearchMember.AutoSize = true;
            this.lblSearchMember.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchMember.Location = new System.Drawing.Point(205, 54);
            this.lblSearchMember.Name = "lblSearchMember";
            this.lblSearchMember.Size = new System.Drawing.Size(106, 20);
            this.lblSearchMember.TabIndex = 31;
            this.lblSearchMember.Text = "Tra cứu SĐT :";
            // 
            // grbMemberList
            // 
            this.grbMemberList.Controls.Add(this.dataGridViewDSHLV);
            this.grbMemberList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbMemberList.Location = new System.Drawing.Point(17, 88);
            this.grbMemberList.Name = "grbMemberList";
            this.grbMemberList.Size = new System.Drawing.Size(988, 135);
            this.grbMemberList.TabIndex = 30;
            this.grbMemberList.TabStop = false;
            this.grbMemberList.Text = "Danh sách";
            // 
            // dataGridViewDSHLV
            // 
            this.dataGridViewDSHLV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDSHLV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDSHLV.Location = new System.Drawing.Point(6, 23);
            this.dataGridViewDSHLV.Name = "dataGridViewDSHLV";
            this.dataGridViewDSHLV.ReadOnly = true;
            this.dataGridViewDSHLV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDSHLV.Size = new System.Drawing.Size(976, 106);
            this.dataGridViewDSHLV.TabIndex = 0;
            this.dataGridViewDSHLV.SelectionChanged += new System.EventHandler(this.dataGridViewDSHLV_SelectionChanged);
            // 
            // lblForm
            // 
            this.lblForm.AutoSize = true;
            this.lblForm.Font = new System.Drawing.Font("Times New Roman", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForm.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblForm.Location = new System.Drawing.Point(306, 5);
            this.lblForm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForm.Name = "lblForm";
            this.lblForm.Size = new System.Drawing.Size(419, 35);
            this.lblForm.TabIndex = 29;
            this.lblForm.Text = "Quản lí huấn luyện viên cá nhân";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtMaHLV);
            this.groupBox1.Controls.Add(this.cbTT);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtBangCap);
            this.groupBox1.Controls.Add(this.cbGT);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbBoMon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.grbPhoto);
            this.groupBox1.Controls.Add(this.btnLuuAnh);
            this.groupBox1.Controls.Add(this.btnChup);
            this.groupBox1.Controls.Add(this.txtLuong);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtDiaChi);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtSDT);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtHoTen);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(17, 238);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(988, 221);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin nhân viên";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(489, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 20);
            this.label10.TabIndex = 36;
            this.label10.Text = "Mã HLV:";
            // 
            // txtMaHLV
            // 
            this.txtMaHLV.Enabled = false;
            this.txtMaHLV.Location = new System.Drawing.Point(566, 176);
            this.txtMaHLV.Name = "txtMaHLV";
            this.txtMaHLV.Size = new System.Drawing.Size(60, 26);
            this.txtMaHLV.TabIndex = 35;
            // 
            // cbTT
            // 
            this.cbTT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTT.FormattingEnabled = true;
            this.cbTT.Location = new System.Drawing.Point(566, 44);
            this.cbTT.Name = "cbTT";
            this.cbTT.Size = new System.Drawing.Size(60, 28);
            this.cbTT.TabIndex = 34;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(483, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 20);
            this.label9.TabIndex = 33;
            this.label9.Text = "Tình trạng:";
            // 
            // txtBangCap
            // 
            this.txtBangCap.Location = new System.Drawing.Point(126, 179);
            this.txtBangCap.Name = "txtBangCap";
            this.txtBangCap.Size = new System.Drawing.Size(141, 26);
            this.txtBangCap.TabIndex = 32;
            // 
            // cbGT
            // 
            this.cbGT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGT.FormattingEnabled = true;
            this.cbGT.Location = new System.Drawing.Point(366, 44);
            this.cbGT.Name = "cbGT";
            this.cbGT.Size = new System.Drawing.Size(97, 28);
            this.cbGT.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 29;
            this.label2.Text = "Bằng cấp:";
            // 
            // cbBoMon
            // 
            this.cbBoMon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBoMon.FormattingEnabled = true;
            this.cbBoMon.Location = new System.Drawing.Point(365, 179);
            this.cbBoMon.Name = "cbBoMon";
            this.cbBoMon.Size = new System.Drawing.Size(98, 28);
            this.cbBoMon.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(291, 182);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Bộ môn:";
            // 
            // grbPhoto
            // 
            this.grbPhoto.Controls.Add(this.pictureBox1);
            this.grbPhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbPhoto.Location = new System.Drawing.Point(647, 25);
            this.grbPhoto.Name = "grbPhoto";
            this.grbPhoto.Size = new System.Drawing.Size(216, 182);
            this.grbPhoto.TabIndex = 26;
            this.grbPhoto.TabStop = false;
            this.grbPhoto.Text = "Ảnh";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(47, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 154);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // btnLuuAnh
            // 
            this.btnLuuAnh.Location = new System.Drawing.Point(878, 129);
            this.btnLuuAnh.Name = "btnLuuAnh";
            this.btnLuuAnh.Size = new System.Drawing.Size(91, 44);
            this.btnLuuAnh.TabIndex = 25;
            this.btnLuuAnh.Text = "Lưu";
            this.btnLuuAnh.UseVisualStyleBackColor = true;
            this.btnLuuAnh.Click += new System.EventHandler(this.btnLuuAnh_Click);
            // 
            // btnChup
            // 
            this.btnChup.Location = new System.Drawing.Point(878, 63);
            this.btnChup.Name = "btnChup";
            this.btnChup.Size = new System.Drawing.Size(91, 44);
            this.btnChup.TabIndex = 24;
            this.btnChup.Text = "Chụp";
            this.btnChup.UseVisualStyleBackColor = true;
            this.btnChup.Click += new System.EventHandler(this.btnChup_Click);
            // 
            // txtLuong
            // 
            this.txtLuong.Location = new System.Drawing.Point(126, 138);
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.Size = new System.Drawing.Size(141, 26);
            this.txtLuong.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(62, 141);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 20);
            this.label8.TabIndex = 22;
            this.label8.Text = "Lương:";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(366, 135);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(260, 26);
            this.txtDiaChi.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(300, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 20);
            this.label7.TabIndex = 20;
            this.label7.Text = "Địa chỉ:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(366, 90);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(260, 26);
            this.txtEmail.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(311, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 20);
            this.label6.TabIndex = 18;
            this.label6.Text = "Email:";
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(126, 90);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(141, 26);
            this.txtSDT.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(75, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "SĐT:";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(126, 44);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(141, 26);
            this.txtHoTen.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "Họ và Tên:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(301, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 18);
            this.label3.TabIndex = 12;
            this.label3.Text = "Giới tính:";
            // 
            // fAdminQuanLiHLV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 549);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnThemHLV);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.txtSearchSDT);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblSearchMember);
            this.Controls.Add(this.grbMemberList);
            this.Controls.Add(this.lblForm);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "fAdminQuanLiHLV";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lí HLV cá nhân";
            this.Load += new System.EventHandler(this.fAdminQuanLiHLV_Load);
            this.grbMemberList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDSHLV)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grbPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnThemHLV;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.TextBox txtSearchSDT;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblSearchMember;
        private System.Windows.Forms.GroupBox grbMemberList;
        private System.Windows.Forms.DataGridView dataGridViewDSHLV;
        private System.Windows.Forms.Label lblForm;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grbPhoto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnLuuAnh;
        private System.Windows.Forms.Button btnChup;
        private System.Windows.Forms.TextBox txtLuong;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbBoMon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBangCap;
        private System.Windows.Forms.ComboBox cbGT;
        private System.Windows.Forms.ComboBox cbTT;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMaHLV;
        private System.Windows.Forms.Label label10;
    }
}