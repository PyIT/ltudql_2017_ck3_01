﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fGiaHanGoiTap : Form
    {
        public fGiaHanGoiTap()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e)
        {
            grbPT.Enabled = !grbPT.Enabled;
            if (!checkBoxThueHLV.Checked)
            {
                txtTongTien.Text = (Convert.ToDouble(txtTongTien.Text) - Convert.ToDouble(txtTienHLV.Text)).ToString();
                txtTienHLV.Text = "0";
            }

        }
        public string IDMB { get; set; }
        public string HoTenMB { get; set; }
        public string DiaChiMB { get; set; }
        public string SDTMB { get; set; }
        public string NgayDKMB { get; set; }
        public string NgayKTMB { get; set; }
        public string GioiTinhMB { get; set; }
        public string GoiTap { get; set; }
        public string  HLV { get; set; }

        double TienGoiTap1Thang;
        //float TongTien;
        private void fGiaHanGoiTap_Load(object sender, EventArgs e)
        {
            TienGoiTap1Thang = 0;
            //TongTien = 0;

            txtHoTen.Text = HoTenMB;
            txtDiaChi.Text = DiaChiMB;
            txtSDT.Text = SDTMB;
            cbxGioiTinh.Text = GioiTinhMB;
            dateTimePickerNgayDangKy.Text = NgayDKMB;
            dateTimePickerNgayKetThuc.Text = NgayKTMB;
            txtTienGoiTap.Text = "0";
            txtTienHLV.Text = "0";
            txtTongTien.Text = "0";

            DataTable t = HLVCaNhanDAO.Instance.LayTenHLV(Convert.ToInt32(HLV));

            string TenHLV = "";
            foreach (DataRow r in t.Rows)
            {
                TenHLV = r["TenPT"].ToString();
            }
            txtHLV.Text = TenHLV;

            //Get Goi tap
            comboGoiTap.DataSource = BoMonDAO.Instance.LayDSBoMon();
            comboGoiTap.DisplayMember = "TenBM";
            comboGoiTap.Text = "";

        }
       
        private void btnGoi1T_Click(object sender, EventArgs e)
        {
            if (comboGoiTap.Text != "")
            {
                dateTimePickerNgayKetThuc.Value = DateTime.Now;
                dateTimePickerNgayKetThuc.Value = dateTimePickerNgayKetThuc.Value.AddMonths(1);
                txtTienGoiTap.Text = (TienGoiTap1Thang * 1).ToString();
                int Tien = int.Parse(txtTienGoiTap.Text);
                int Bac = int.Parse(txtTienHLV.Text);
                txtTongTien.Text = (Tien + Bac).ToString();
            }
            else
            {
                MessageBox.Show("Gói tập đang trống", "Ui, có gì đó không đúng", MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void btnGoi3T_Click(object sender, EventArgs e)
        {
            if (comboGoiTap.Text != "")
            {
                dateTimePickerNgayKetThuc.Value = DateTime.Now;
                dateTimePickerNgayKetThuc.Value = dateTimePickerNgayKetThuc.Value.AddMonths(3);
                txtTienGoiTap.Text = (TienGoiTap1Thang * 2.5).ToString();
                int Tien = int.Parse(txtTienGoiTap.Text);
                int Bac = int.Parse(txtTienHLV.Text);
                txtTongTien.Text = (Tien + Bac).ToString();
            }
            else
            {
                MessageBox.Show("Gói tập đang trống", "Ui, có gì đó không đúng", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnGoi6T_Click(object sender, EventArgs e)
        {
            if (comboGoiTap.Text != "")
            {
                dateTimePickerNgayKetThuc.Value = DateTime.Now;
                dateTimePickerNgayKetThuc.Value = dateTimePickerNgayKetThuc.Value.AddMonths(6);
                txtTienGoiTap.Text = (TienGoiTap1Thang * 4).ToString();
                int Tien = int.Parse(txtTienGoiTap.Text);
                int Bac = int.Parse(txtTienHLV.Text);
                txtTongTien.Text = (Tien + Bac).ToString();
            }
            else
            {
                MessageBox.Show("Gói tập đang trống", "Ui, có gì đó không đúng", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        public int b = 0;
        private void btnPT1T_Click(object sender, EventArgs e)
        {
            if (cbxPT.Text != "")
            {
                dateTimePickerNDKPT.Value = DateTime.Now;
                dateTimePickerNKTPT.Value = dateTimePickerNKTPT.Value.AddMonths(1);
                txtTienHLV.Text = (TienGoiTap1Thang * 1 * 7).ToString();
                int Tien = int.Parse(txtTienGoiTap.Text);
                int Bac = int.Parse(txtTienHLV.Text);
                txtTongTien.Text = (Tien + Bac).ToString();
                b = 1;
            }
            else
            {
                MessageBox.Show("Huấn luyện viên đang trống", "Ui, có gì đó không đúng", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnPT3T_Click(object sender, EventArgs e)
        {
            if (cbxPT.Text != "")
            {
                dateTimePickerNDKPT.Value = DateTime.Now;
                dateTimePickerNKTPT.Value = dateTimePickerNKTPT.Value.AddMonths(3);
                txtTienHLV.Text = (TienGoiTap1Thang * 2.5 * 7).ToString();
                int Tien = int.Parse(txtTienGoiTap.Text);
                int Bac = int.Parse(txtTienHLV.Text);
                txtTongTien.Text = (Tien + Bac).ToString();
                b = 1;
            }
            else
            {
                MessageBox.Show("Huấn luyện viên đang trống", "Ui, có gì đó không đúng", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnPT6T_Click(object sender, EventArgs e)
        {
            if (cbxPT.Text != "")
            {
                dateTimePickerNDKPT.Value = DateTime.Now;
                dateTimePickerNKTPT.Value = dateTimePickerNKTPT.Value.AddMonths(6);
                txtTienHLV.Text = (TienGoiTap1Thang * 4 * 7).ToString();
                int Tien = int.Parse(txtTienGoiTap.Text);
                int Bac = int.Parse(txtTienHLV.Text);
                txtTongTien.Text = (Tien + Bac).ToString();
                b = 1;
            }
            else
            {
                MessageBox.Show("Huấn luyện viên đang trống", "Ui, có gì đó không đúng", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        string CreateDate (string value)
        {
            // Get year, month, day
            string[] resultGetDate = value.Split(new char[] { '/' });
            int month = Convert.ToInt32(resultGetDate[0]);
            int day = Convert.ToInt32(resultGetDate[1]);
            string[] rsYear = resultGetDate[2].Split(new char[] { ' ' });
            int year = Convert.ToInt32(rsYear[0]);
            // Get DATE KT
            return year.ToString() + '-' + month.ToString() + '-' + day.ToString();
        }
        private void btnInHoaDon_Click(object sender, EventArgs e)
        {
           
            if (txtTongTien.Text != "0")
            {
                if (a == true)
                {
                    if (cbxPT.Text != "")
                    {

                        if (b == 1)
                        {

                            DataTable tb = HLVCaNhanDAO.Instance.LayIdHLV(cbxPT.SelectedValue.ToString());
                            int HLV = 0;
                            foreach (DataRow r in tb.Rows)
                            {
                                HLV = Convert.ToInt32(r["MaPT"]);
                            }

                            // Get DATE KT
                            string NgayKT = CreateDate(dateTimePickerNgayKetThuc.Value.ToString());

                            // Get DATE KT HLV
                            string NgayKTHLV = CreateDate(dateTimePickerNKTPT.Value.ToString());

                            DAO.GiaHanGoiTapDAO.Instance.ThemGoiTap(HLV, IDMB, comboGoiTap.Text, NgayKT, NgayKTHLV);
                            string ChiTiet = "";
                            ChiTiet += "Ten KH: " + txtHoTen.Text + " - " + "SDT: " + txtSDT.Text + " - " + "Ngay Ket Thuc: " + dateTimePickerNgayDangKy.Value.ToString();
                            DAO.GiaHanGoiTapDAO.Instance.ThemHoaDon(ChiTiet, txtTongTien.Text);
                            MessageBox.Show("In Hóa Đơn Thành Công");
                            this.Close();
                        }
                        else
                            MessageBox.Show("Chưa Chọn Thời Gian Của PT");
                    }
                    else
                        MessageBox.Show("Cần Chọn PT");
                }
                else if (a == false)
                {
                    string ChiTiet = "";
                    ChiTiet += "Ten KH: " + txtHoTen.Text + " - " + "SDT: " + txtSDT.Text + " - " + "Ngay Thanh Toan: " + dateTimePickerNgayDangKy.Value.ToString();
                    DAO.GiaHanGoiTapDAO.Instance.ThemHoaDon(ChiTiet, txtTongTien.Text);
                    MessageBox.Show("Đã In Hóa Đơn");
                }
            }
            else
                MessageBox.Show("Chưa Gia Hạn Gói Tập Hoặc Gói Tập Có PT");
           
        }
        public bool a = false;
        private void checkBoxThueHLV_CheckedChanged(object sender, EventArgs e)
        {
            a = true;
        }

        private void comboGoiTap_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable tb = BoMonDAO.Instance.LayGiaBM(comboGoiTap.Text);
            double Gia = 0;
            foreach (DataRow r in tb.Rows)
            {
                Gia = Convert.ToDouble(r["GiaBM"]);
            }
            TienGoiTap1Thang = Gia;

            //txtTongTien.Text = (Convert.ToDouble(txtTongTien.Text)-Convert.ToDouble(txtTienGoiTap.Text)).ToString();
            txtTongTien.Text = "0";
            txtTienGoiTap.Text = "0";

            txtTienHLV.Text = "0";


            cbxPT.DataSource = DAO.GiaHanGoiTapDAO.Instance.LayDanhSachPT(comboGoiTap.Text);
            cbxPT.DisplayMember = "TenPT";
            cbxPT.ValueMember = "TenPT";
            cbxPT.Text = "";
        }
    }
}
