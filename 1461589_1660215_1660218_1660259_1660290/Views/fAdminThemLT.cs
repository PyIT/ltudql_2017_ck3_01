﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fAdminThemLT : Form
    {
        public fAdminThemLT()
        {
            InitializeComponent();
        }

        private void fAdminThemLT_Load(object sender, EventArgs e)
        {
            cbGioiTinh.Items.Clear();
            cbGioiTinh.Items.Add("Nữ");
            cbGioiTinh.Items.Add("Nam");
            cbGioiTinh.Text = "Nữ";
        }

        private void btnChup_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        // Check string all num
        bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtTenDangNhap.Text != "" && txtMatKhau.Text != "" && txtHoTen.Text != "" & txtDiaChi.Text != "" && txtEmail.Text != "" && txtSDT.Text != "" && txtLuong.Text != "" && cbGioiTinh.Text != "" && txtBangCap.Text != "")
            {
                if (IsNumber(txtSDT.Text))
                {
                    if (IsNumber(txtLuong.Text))
                    {
                        // Xu ly mat khau
                        //Tạo MD5 
                        MD5 mh = MD5.Create();
                        //Chuyển kiểu chuổi thành kiểu byte
                        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(txtMatKhau.Text);
                        //mã hóa chuỗi đã chuyển
                        byte[] hash = mh.ComputeHash(inputBytes);
                        //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
                        StringBuilder MatKhauHash = new StringBuilder();
                        for (int i = 0; i < hash.Length; i++)
                        {
                            MatKhauHash.Append(hash[i].ToString("x2"));
                        }

                        // Xu ly ngay sinh
                        // Get year, month, day
                        string[] resultGetDate = dateTimePickerNgaySinh.Value.ToString().Split(new char[] { '/' });
                        int month = Convert.ToInt32(resultGetDate[0]);
                        int day = Convert.ToInt32(resultGetDate[1]);
                        string[] rsYear = resultGetDate[2].Split(new char[] { ' ' });
                        int year = Convert.ToInt32(rsYear[0]);
                        // Get DATE
                        string NgaySinh = year.ToString() + '-' + month.ToString() + '-' + day.ToString();

                        //Xu li gioi tinh
                        // Get sex
                        int sex = 0;
                        if (cbGioiTinh.Text == "Nam")
                            sex = 1;

                        if (TaiKhoanDAO.Instance.ThemLT(txtTenDangNhap.Text, txtHoTen.Text, sex,txtLuong.Text, txtBangCap.Text, txtDiaChi.Text, txtEmail.Text, txtSDT.Text, MatKhauHash.ToString(),NgaySinh))
                        {
                            MessageBox.Show("Thêm tài khoản lễ tân thành công ", "Thành công", MessageBoxButtons.OK);
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Lương chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Số điện thoại chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Cần nhập đầy đủ thông tin", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
