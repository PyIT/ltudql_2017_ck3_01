﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fAdminQuanLiHLV : Form
    {

        
        public fAdminQuanLiHLV()
        {
            InitializeComponent();
        }

        private void btnThemHLV_Click(object sender, EventArgs e)
        {
            fAdminThemHLV f = new fAdminThemHLV();
            f.ShowDialog();
            fAdminQuanLiHLV_Load(sender, e); // Load lai Form
        }

        private void fAdminQuanLiHLV_Load(object sender, EventArgs e)
        {
            // Get DS Bo Mon tu DB, Set vaule control Bo Mon
            cbBoMon.DataSource = BoMonDAO.Instance.LayDSBoMon();
            cbBoMon.DisplayMember = "TenBM";

            // Set control Gioi Tinh
            cbGT.Items.Clear();
            cbGT.Items.Add("Nữ");
            cbGT.Items.Add("Nam");
            cbGT.Text = "Nam";

            // Set control Gioi Tinh
            cbTT.Items.Clear();
            cbTT.Items.Add("On");
            cbTT.Items.Add("Off");
            cbTT.Text = "On";

            dataGridViewDSHLV.DataSource = HLVCaNhanDAO.Instance.LayThongTinHLV();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dataGridViewDSHLV.DataSource = HLVCaNhanDAO.Instance.LayHLVTuSDT(txtSearchSDT.Text);
        }

        private void dataGridViewDSHLV_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int index = dataGridViewDSHLV.CurrentRow.Index;


                txtMaHLV.Text = dataGridViewDSHLV.Rows[index].Cells[0].Value.ToString();

                txtHoTen.Text = dataGridViewDSHLV.Rows[index].Cells[1].Value.ToString();

                //Set control GioiTinh
                if (dataGridViewDSHLV.Rows[index].Cells[2].Value.ToString() == "True")
                    cbGT.Text = @"Nam";
                else cbGT.Text = @"Nữ";
                //Set control Trang thai
                if (dataGridViewDSHLV.Rows[index].Cells[9].Value.ToString() == "True")
                    cbTT.Text = @"On";
                else cbTT.Text = @"Off";


                txtEmail.Text = dataGridViewDSHLV.Rows[index].Cells[3].Value.ToString();
                txtSDT.Text = dataGridViewDSHLV.Rows[index].Cells[4].Value.ToString();
                txtDiaChi.Text = dataGridViewDSHLV.Rows[index].Cells[5].Value.ToString();
                cbBoMon.Text = dataGridViewDSHLV.Rows[index].Cells[6].Value.ToString();
                txtLuong.Text = dataGridViewDSHLV.Rows[index].Cells[7].Value.ToString();
                txtBangCap.Text = dataGridViewDSHLV.Rows[index].Cells[8].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Không thể click chọn ở đây!", "Ui, có gì đó không đúng!", MessageBoxButtons.OK);
            }
        }

        private void btnChup_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnLuuAnh_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {

            if (txtMaHLV.Text != "" && txtHoTen.Text != "" && txtSDT.Text != "" && txtDiaChi.Text != "" && txtEmail.Text != "" && txtLuong.Text != "" && txtBangCap.Text != "" && cbGT.Text != "" && cbTT.Text != "" && cbBoMon.Text != "")
            {
                if (IsNumber(txtSDT.Text))
                {
                    if (IsNumber(txtLuong.Text))
                    {
                        int GT = 0;
                        if (cbGT.Text == "Nam") GT = 1;

                        int TT = 0;
                        if (cbTT.Text == "On") TT = 1;

                        if (HLVCaNhanDAO.Instance.ChinhSuaThongTinHLV(txtMaHLV.Text, txtHoTen.Text, GT, txtLuong.Text, txtBangCap.Text, txtDiaChi.Text, txtEmail.Text, txtSDT.Text,cbBoMon.Text, TT))
                        {
                            MessageBox.Show("Lưu chỉnh sửa thành công !", "Thành công", MessageBoxButtons.OK);
                            fAdminQuanLiHLV_Load(sender, e);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Lương chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Số điện thoại chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Cần nhập đầy đủ thông tin", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
