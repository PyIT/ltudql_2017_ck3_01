﻿namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    partial class fGiaHanGoiTap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblForm = new System.Windows.Forms.Label();
            this.grbMemberInfor = new System.Windows.Forms.GroupBox();
            this.comboGoiTap = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtHLV = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerNgayKetThuc = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerNgayDangKy = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxGioiTinh = new System.Windows.Forms.ComboBox();
            this.lblSex = new System.Windows.Forms.Label();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.lblFullName = new System.Windows.Forms.Label();
            this.btnGoi1T = new System.Windows.Forms.Button();
            this.btnGoi3T = new System.Windows.Forms.Button();
            this.btnGoi6T = new System.Windows.Forms.Button();
            this.checkBoxThueHLV = new System.Windows.Forms.CheckBox();
            this.grbPT = new System.Windows.Forms.GroupBox();
            this.btnPT6T = new System.Windows.Forms.Button();
            this.dateTimePickerNKTPT = new System.Windows.Forms.DateTimePicker();
            this.btnPT3T = new System.Windows.Forms.Button();
            this.dateTimePickerNDKPT = new System.Windows.Forms.DateTimePicker();
            this.btnPT1T = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbxPT = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnInHoaDon = new System.Windows.Forms.Button();
            this.txtTongTien = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTienHLV = new System.Windows.Forms.TextBox();
            this.txtTienGoiTap = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.grbMemberInfor.SuspendLayout();
            this.grbPT.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblForm
            // 
            this.lblForm.AutoSize = true;
            this.lblForm.Font = new System.Drawing.Font("Times New Roman", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForm.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblForm.Location = new System.Drawing.Point(350, 9);
            this.lblForm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForm.Name = "lblForm";
            this.lblForm.Size = new System.Drawing.Size(209, 35);
            this.lblForm.TabIndex = 2;
            this.lblForm.Text = "Gia hạn gói tập";
            // 
            // grbMemberInfor
            // 
            this.grbMemberInfor.Controls.Add(this.comboGoiTap);
            this.grbMemberInfor.Controls.Add(this.label6);
            this.grbMemberInfor.Controls.Add(this.txtHLV);
            this.grbMemberInfor.Controls.Add(this.label5);
            this.grbMemberInfor.Controls.Add(this.dateTimePickerNgayKetThuc);
            this.grbMemberInfor.Controls.Add(this.dateTimePickerNgayDangKy);
            this.grbMemberInfor.Controls.Add(this.label4);
            this.grbMemberInfor.Controls.Add(this.label3);
            this.grbMemberInfor.Controls.Add(this.txtSDT);
            this.grbMemberInfor.Controls.Add(this.label1);
            this.grbMemberInfor.Controls.Add(this.txtDiaChi);
            this.grbMemberInfor.Controls.Add(this.label2);
            this.grbMemberInfor.Controls.Add(this.cbxGioiTinh);
            this.grbMemberInfor.Controls.Add(this.lblSex);
            this.grbMemberInfor.Controls.Add(this.txtHoTen);
            this.grbMemberInfor.Controls.Add(this.lblFullName);
            this.grbMemberInfor.Location = new System.Drawing.Point(21, 62);
            this.grbMemberInfor.Name = "grbMemberInfor";
            this.grbMemberInfor.Size = new System.Drawing.Size(700, 190);
            this.grbMemberInfor.TabIndex = 7;
            this.grbMemberInfor.TabStop = false;
            this.grbMemberInfor.Text = "Thông tin khách hàng";
            // 
            // comboGoiTap
            // 
            this.comboGoiTap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGoiTap.FormattingEnabled = true;
            this.comboGoiTap.Location = new System.Drawing.Point(518, 110);
            this.comboGoiTap.Name = "comboGoiTap";
            this.comboGoiTap.Size = new System.Drawing.Size(151, 28);
            this.comboGoiTap.TabIndex = 28;
            this.comboGoiTap.SelectedIndexChanged += new System.EventHandler(this.comboGoiTap_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(452, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 20);
            this.label6.TabIndex = 22;
            this.label6.Text = "Gói tập:";
            // 
            // txtHLV
            // 
            this.txtHLV.Enabled = false;
            this.txtHLV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHLV.Location = new System.Drawing.Point(518, 147);
            this.txtHLV.Name = "txtHLV";
            this.txtHLV.ReadOnly = true;
            this.txtHLV.Size = new System.Drawing.Size(151, 26);
            this.txtHLV.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(472, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 20);
            this.label5.TabIndex = 20;
            this.label5.Text = "HLV:";
            // 
            // dateTimePickerNgayKetThuc
            // 
            this.dateTimePickerNgayKetThuc.Enabled = false;
            this.dateTimePickerNgayKetThuc.Location = new System.Drawing.Point(125, 146);
            this.dateTimePickerNgayKetThuc.Name = "dateTimePickerNgayKetThuc";
            this.dateTimePickerNgayKetThuc.Size = new System.Drawing.Size(289, 26);
            this.dateTimePickerNgayKetThuc.TabIndex = 19;
            // 
            // dateTimePickerNgayDangKy
            // 
            this.dateTimePickerNgayDangKy.Enabled = false;
            this.dateTimePickerNgayDangKy.Location = new System.Drawing.Point(125, 108);
            this.dateTimePickerNgayDangKy.Name = "dateTimePickerNgayDangKy";
            this.dateTimePickerNgayDangKy.Size = new System.Drawing.Size(289, 26);
            this.dateTimePickerNgayDangKy.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 20);
            this.label4.TabIndex = 16;
            this.label4.Text = "Ngày kết thúc:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "Ngày đăng ký:";
            // 
            // txtSDT
            // 
            this.txtSDT.Enabled = false;
            this.txtSDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSDT.Location = new System.Drawing.Point(518, 67);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(151, 26);
            this.txtSDT.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(470, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "SĐT:";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Enabled = false;
            this.txtDiaChi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(80, 67);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(334, 26);
            this.txtDiaChi.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Địa chỉ:";
            // 
            // cbxGioiTinh
            // 
            this.cbxGioiTinh.Enabled = false;
            this.cbxGioiTinh.FormattingEnabled = true;
            this.cbxGioiTinh.Location = new System.Drawing.Point(518, 27);
            this.cbxGioiTinh.Name = "cbxGioiTinh";
            this.cbxGioiTinh.Size = new System.Drawing.Size(79, 28);
            this.cbxGioiTinh.TabIndex = 9;
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Location = new System.Drawing.Point(446, 30);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(71, 20);
            this.lblSex.TabIndex = 8;
            this.lblSex.Text = "Giới tính:";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Enabled = false;
            this.txtHoTen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Location = new System.Drawing.Point(80, 26);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(334, 26);
            this.txtHoTen.TabIndex = 7;
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = true;
            this.lblFullName.Location = new System.Drawing.Point(18, 30);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(61, 20);
            this.lblFullName.TabIndex = 0;
            this.lblFullName.Text = "Họ tên:";
            // 
            // btnGoi1T
            // 
            this.btnGoi1T.Location = new System.Drawing.Point(737, 71);
            this.btnGoi1T.Name = "btnGoi1T";
            this.btnGoi1T.Size = new System.Drawing.Size(116, 50);
            this.btnGoi1T.TabIndex = 8;
            this.btnGoi1T.Text = "1 Tháng";
            this.btnGoi1T.UseVisualStyleBackColor = true;
            this.btnGoi1T.Click += new System.EventHandler(this.btnGoi1T_Click);
            // 
            // btnGoi3T
            // 
            this.btnGoi3T.Location = new System.Drawing.Point(737, 136);
            this.btnGoi3T.Name = "btnGoi3T";
            this.btnGoi3T.Size = new System.Drawing.Size(116, 50);
            this.btnGoi3T.TabIndex = 9;
            this.btnGoi3T.Text = "3 Tháng";
            this.btnGoi3T.UseVisualStyleBackColor = true;
            this.btnGoi3T.Click += new System.EventHandler(this.btnGoi3T_Click);
            // 
            // btnGoi6T
            // 
            this.btnGoi6T.Location = new System.Drawing.Point(737, 202);
            this.btnGoi6T.Name = "btnGoi6T";
            this.btnGoi6T.Size = new System.Drawing.Size(116, 50);
            this.btnGoi6T.TabIndex = 10;
            this.btnGoi6T.Text = "6 Tháng";
            this.btnGoi6T.UseVisualStyleBackColor = true;
            this.btnGoi6T.Click += new System.EventHandler(this.btnGoi6T_Click);
            // 
            // checkBoxThueHLV
            // 
            this.checkBoxThueHLV.AutoSize = true;
            this.checkBoxThueHLV.Location = new System.Drawing.Point(41, 267);
            this.checkBoxThueHLV.Name = "checkBoxThueHLV";
            this.checkBoxThueHLV.Size = new System.Drawing.Size(161, 24);
            this.checkBoxThueHLV.TabIndex = 11;
            this.checkBoxThueHLV.Text = "Thuê HLV cá nhân";
            this.checkBoxThueHLV.UseVisualStyleBackColor = true;
            this.checkBoxThueHLV.CheckedChanged += new System.EventHandler(this.checkBoxThueHLV_CheckedChanged);
            this.checkBoxThueHLV.CheckStateChanged += new System.EventHandler(this.checkBox1_CheckStateChanged);
            // 
            // grbPT
            // 
            this.grbPT.Controls.Add(this.btnPT6T);
            this.grbPT.Controls.Add(this.dateTimePickerNKTPT);
            this.grbPT.Controls.Add(this.btnPT3T);
            this.grbPT.Controls.Add(this.dateTimePickerNDKPT);
            this.grbPT.Controls.Add(this.btnPT1T);
            this.grbPT.Controls.Add(this.label9);
            this.grbPT.Controls.Add(this.label10);
            this.grbPT.Controls.Add(this.cbxPT);
            this.grbPT.Controls.Add(this.label13);
            this.grbPT.Enabled = false;
            this.grbPT.Location = new System.Drawing.Point(21, 297);
            this.grbPT.Name = "grbPT";
            this.grbPT.Size = new System.Drawing.Size(700, 157);
            this.grbPT.TabIndex = 24;
            this.grbPT.TabStop = false;
            this.grbPT.Text = "Thông tin PT";
            // 
            // btnPT6T
            // 
            this.btnPT6T.Location = new System.Drawing.Point(507, 95);
            this.btnPT6T.Name = "btnPT6T";
            this.btnPT6T.Size = new System.Drawing.Size(116, 50);
            this.btnPT6T.TabIndex = 27;
            this.btnPT6T.Text = "6 Tháng";
            this.btnPT6T.UseVisualStyleBackColor = true;
            this.btnPT6T.Click += new System.EventHandler(this.btnPT6T_Click);
            // 
            // dateTimePickerNKTPT
            // 
            this.dateTimePickerNKTPT.Enabled = false;
            this.dateTimePickerNKTPT.Location = new System.Drawing.Point(125, 108);
            this.dateTimePickerNKTPT.Name = "dateTimePickerNKTPT";
            this.dateTimePickerNKTPT.Size = new System.Drawing.Size(289, 26);
            this.dateTimePickerNKTPT.TabIndex = 19;
            // 
            // btnPT3T
            // 
            this.btnPT3T.Location = new System.Drawing.Point(578, 20);
            this.btnPT3T.Name = "btnPT3T";
            this.btnPT3T.Size = new System.Drawing.Size(116, 50);
            this.btnPT3T.TabIndex = 26;
            this.btnPT3T.Text = "3 Tháng";
            this.btnPT3T.UseVisualStyleBackColor = true;
            this.btnPT3T.Click += new System.EventHandler(this.btnPT3T_Click);
            // 
            // dateTimePickerNDKPT
            // 
            this.dateTimePickerNDKPT.Enabled = false;
            this.dateTimePickerNDKPT.Location = new System.Drawing.Point(125, 69);
            this.dateTimePickerNDKPT.Name = "dateTimePickerNDKPT";
            this.dateTimePickerNDKPT.Size = new System.Drawing.Size(289, 26);
            this.dateTimePickerNDKPT.TabIndex = 18;
            // 
            // btnPT1T
            // 
            this.btnPT1T.Location = new System.Drawing.Point(436, 20);
            this.btnPT1T.Name = "btnPT1T";
            this.btnPT1T.Size = new System.Drawing.Size(116, 50);
            this.btnPT1T.TabIndex = 25;
            this.btnPT1T.Text = "1 Tháng";
            this.btnPT1T.UseVisualStyleBackColor = true;
            this.btnPT1T.Click += new System.EventHandler(this.btnPT1T_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Location = new System.Drawing.Point(14, 111);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 20);
            this.label9.TabIndex = 16;
            this.label9.Text = "Ngày kết thúc:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Location = new System.Drawing.Point(16, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 20);
            this.label10.TabIndex = 14;
            this.label10.Text = "Ngày đăng ký:";
            // 
            // cbxPT
            // 
            this.cbxPT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPT.FormattingEnabled = true;
            this.cbxPT.Location = new System.Drawing.Point(125, 27);
            this.cbxPT.Name = "cbxPT";
            this.cbxPT.Size = new System.Drawing.Size(142, 28);
            this.cbxPT.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(91, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 20);
            this.label13.TabIndex = 8;
            this.label13.Text = "PT:";
            // 
            // btnInHoaDon
            // 
            this.btnInHoaDon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInHoaDon.Location = new System.Drawing.Point(650, 469);
            this.btnInHoaDon.Name = "btnInHoaDon";
            this.btnInHoaDon.Size = new System.Drawing.Size(203, 78);
            this.btnInHoaDon.TabIndex = 28;
            this.btnInHoaDon.Text = "In Hóa Đơn";
            this.btnInHoaDon.UseVisualStyleBackColor = true;
            this.btnInHoaDon.Click += new System.EventHandler(this.btnInHoaDon_Click);
            // 
            // txtTongTien
            // 
            this.txtTongTien.Enabled = false;
            this.txtTongTien.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTien.Location = new System.Drawing.Point(230, 526);
            this.txtTongTien.Name = "txtTongTien";
            this.txtTongTien.Size = new System.Drawing.Size(205, 30);
            this.txtTongTien.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(114, 529);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 25);
            this.label7.TabIndex = 24;
            this.label7.Text = "Tổng tiền:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Enabled = false;
            this.label8.Location = new System.Drawing.Point(441, 533);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 20);
            this.label8.TabIndex = 24;
            this.label8.Text = "VNĐ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(123, 494);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 25);
            this.label11.TabIndex = 29;
            this.label11.Text = "Tiền HLV:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(114, 460);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 25);
            this.label12.TabIndex = 30;
            this.label12.Text = "Tiền gói tập:";
            // 
            // txtTienHLV
            // 
            this.txtTienHLV.Enabled = false;
            this.txtTienHLV.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienHLV.Location = new System.Drawing.Point(230, 491);
            this.txtTienHLV.Name = "txtTienHLV";
            this.txtTienHLV.Size = new System.Drawing.Size(205, 30);
            this.txtTienHLV.TabIndex = 31;
            // 
            // txtTienGoiTap
            // 
            this.txtTienGoiTap.Enabled = false;
            this.txtTienGoiTap.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienGoiTap.Location = new System.Drawing.Point(230, 457);
            this.txtTienGoiTap.Name = "txtTienGoiTap";
            this.txtTienGoiTap.Size = new System.Drawing.Size(205, 30);
            this.txtTienGoiTap.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Enabled = false;
            this.label14.Location = new System.Drawing.Point(441, 464);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 20);
            this.label14.TabIndex = 33;
            this.label14.Text = "VNĐ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Enabled = false;
            this.label15.Location = new System.Drawing.Point(441, 499);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 20);
            this.label15.TabIndex = 34;
            this.label15.Text = "VNĐ";
            // 
            // fGiaHanGoiTap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 576);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtTienGoiTap);
            this.Controls.Add(this.txtTienHLV);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtTongTien);
            this.Controls.Add(this.btnInHoaDon);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.grbPT);
            this.Controls.Add(this.checkBoxThueHLV);
            this.Controls.Add(this.btnGoi6T);
            this.Controls.Add(this.btnGoi3T);
            this.Controls.Add(this.btnGoi1T);
            this.Controls.Add(this.grbMemberInfor);
            this.Controls.Add(this.lblForm);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "fGiaHanGoiTap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gia hạn gói tập";
            this.Load += new System.EventHandler(this.fGiaHanGoiTap_Load);
            this.grbMemberInfor.ResumeLayout(false);
            this.grbMemberInfor.PerformLayout();
            this.grbPT.ResumeLayout(false);
            this.grbPT.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblForm;
        private System.Windows.Forms.GroupBox grbMemberInfor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtHLV;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePickerNgayKetThuc;
        private System.Windows.Forms.DateTimePicker dateTimePickerNgayDangKy;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxGioiTinh;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.Label lblFullName;
        private System.Windows.Forms.Button btnGoi1T;
        private System.Windows.Forms.Button btnGoi3T;
        private System.Windows.Forms.Button btnGoi6T;
        private System.Windows.Forms.CheckBox checkBoxThueHLV;
        private System.Windows.Forms.GroupBox grbPT;
        private System.Windows.Forms.Button btnPT6T;
        private System.Windows.Forms.DateTimePicker dateTimePickerNKTPT;
        private System.Windows.Forms.Button btnPT3T;
        private System.Windows.Forms.DateTimePicker dateTimePickerNDKPT;
        private System.Windows.Forms.Button btnPT1T;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbxPT;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnInHoaDon;
        private System.Windows.Forms.TextBox txtTongTien;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtTienHLV;
        private System.Windows.Forms.TextBox txtTienGoiTap;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboGoiTap;
    }
}