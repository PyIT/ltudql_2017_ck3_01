﻿using _1461589_1660215_1660218_1660259_1660290.DTO;
using _1461589_1660215_1660218_1660259_1660290.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290
{
    public partial class fManage : Form
    {
        private TaiKhoanDTO login;

        internal TaiKhoanDTO Login {
            get
            {
                return login;
            }
            set
            {
                login = value;
                ChangAccount(login.LoaiTK);
            }
        }

        internal fManage(TaiKhoanDTO acc)
        {
            InitializeComponent();
            this.Login = acc;
        }

        void ChangAccount (bool type)
        {
            adminToolStripMenuAdmin.Enabled = type == true;
        }

        private void fManage_Load(object sender, EventArgs e)
        {
             
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            //this.Hide();
            fSignUp f = new fSignUp();
            f.ShowDialog();
            //this.Show();
        }

        private void btnMember_Click(object sender, EventArgs e)
        {
            fMember f = new fMember();
            f.ShowDialog();
        }

        private void btnDay_Click(object sender, EventArgs e)
        {
            fDay f = new fDay();
            f.ShowDialog();
        }

        private void đổiMậtKhẩuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fChangePasswordAccount f = new fChangePasswordAccount(this.Login);
            f.ShowDialog();
        }

        private void đổiThôngTinCáNhânToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fChangeInformationAccount f = new fChangeInformationAccount(this.Login);
            f.ShowDialog();
        }

        private void quảnLíLễTânToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fAdminQuanLiLT f = new fAdminQuanLiLT();
            f.ShowDialog();
        }

        private void quảnLíHuấnLuyệnViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fAdminQuanLiHLV f = new fAdminQuanLiHLV();
            f.ShowDialog();
        }

        private void trợGiúpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fHelp f = new fHelp();
            f.ShowDialog();
        }

        private void quảnLíGóiTậpToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            fAdminQLP_QLGoiTap f = new fAdminQLP_QLGoiTap();
            f.ShowDialog();
        }

        private void quảnLíThiếtBịToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fAdminQLP_QLThietBi f = new fAdminQLP_QLThietBi();
            f.ShowDialog();
        }

        private void quảnLíLịchHLVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fAdminQLP_QuanLiLichHLV f = new fAdminQLP_QuanLiLichHLV();
            f.ShowDialog();
        }

        private void quảnLíBánHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fAdminQLP_QuanLiHoaDon f = new fAdminQLP_QuanLiHoaDon();
            f.ShowDialog();
        }

        private void btnStore_Click(object sender, EventArgs e)
        {
            fStore f = new fStore();
            f.ShowDialog();
        }

        private void fManage_Load_1(object sender, EventArgs e)
        {

        }

        private void thuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void chiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void thôngKêLươngNhânViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void thốngKêLợiNhuậnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void quảnLíStoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fAdminQLStore f = new fAdminQLStore();
            f.ShowDialog();
        }
    }
}
