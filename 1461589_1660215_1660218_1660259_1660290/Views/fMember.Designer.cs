﻿namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    partial class fMember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblForm = new System.Windows.Forms.Label();
            this.grbMemberList = new System.Windows.Forms.GroupBox();
            this.dataGridViewDanhSachThanhVien = new System.Windows.Forms.DataGridView();
            this.lblSearchMember = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtTraCuuKH = new System.Windows.Forms.TextBox();
            this.grbMemberInfor = new System.Windows.Forms.GroupBox();
            this.txtGoiTap = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtHLV = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerNgayKetThuc = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerNgayDangKi = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSDTKH = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDiaChiKH = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxGioiTinhKH = new System.Windows.Forms.ComboBox();
            this.lblSex = new System.Windows.Forms.Label();
            this.txtHoTenKH = new System.Windows.Forms.TextBox();
            this.lblFullName = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnGiaHan = new System.Windows.Forms.Button();
            this.btnLuuCS = new System.Windows.Forms.Button();
            this.grbMemberList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDanhSachThanhVien)).BeginInit();
            this.grbMemberInfor.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblForm
            // 
            this.lblForm.AutoSize = true;
            this.lblForm.Font = new System.Drawing.Font("Times New Roman", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForm.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblForm.Location = new System.Drawing.Point(327, 18);
            this.lblForm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForm.Name = "lblForm";
            this.lblForm.Size = new System.Drawing.Size(282, 35);
            this.lblForm.TabIndex = 1;
            this.lblForm.Text = "Tài khoản thành viên";
            // 
            // grbMemberList
            // 
            this.grbMemberList.Controls.Add(this.dataGridViewDanhSachThanhVien);
            this.grbMemberList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbMemberList.Location = new System.Drawing.Point(12, 120);
            this.grbMemberList.Name = "grbMemberList";
            this.grbMemberList.Size = new System.Drawing.Size(874, 235);
            this.grbMemberList.TabIndex = 2;
            this.grbMemberList.TabStop = false;
            this.grbMemberList.Text = "Danh sách";
            // 
            // dataGridViewDanhSachThanhVien
            // 
            this.dataGridViewDanhSachThanhVien.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDanhSachThanhVien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDanhSachThanhVien.Location = new System.Drawing.Point(6, 23);
            this.dataGridViewDanhSachThanhVien.Name = "dataGridViewDanhSachThanhVien";
            this.dataGridViewDanhSachThanhVien.ReadOnly = true;
            this.dataGridViewDanhSachThanhVien.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDanhSachThanhVien.Size = new System.Drawing.Size(862, 206);
            this.dataGridViewDanhSachThanhVien.TabIndex = 0;
            this.dataGridViewDanhSachThanhVien.SelectionChanged += new System.EventHandler(this.dataGridViewDanhSachThanhVien_SelectionChanged);
            // 
            // lblSearchMember
            // 
            this.lblSearchMember.AutoSize = true;
            this.lblSearchMember.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchMember.Location = new System.Drawing.Point(70, 82);
            this.lblSearchMember.Name = "lblSearchMember";
            this.lblSearchMember.Size = new System.Drawing.Size(106, 20);
            this.lblSearchMember.TabIndex = 3;
            this.lblSearchMember.Text = "Tra cứu SĐT :";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(697, 74);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(103, 37);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Tra cứu";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtTraCuuKH
            // 
            this.txtTraCuuKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTraCuuKH.Location = new System.Drawing.Point(182, 79);
            this.txtTraCuuKH.Name = "txtTraCuuKH";
            this.txtTraCuuKH.Size = new System.Drawing.Size(499, 26);
            this.txtTraCuuKH.TabIndex = 5;
            // 
            // grbMemberInfor
            // 
            this.grbMemberInfor.Controls.Add(this.txtGoiTap);
            this.grbMemberInfor.Controls.Add(this.label6);
            this.grbMemberInfor.Controls.Add(this.txtHLV);
            this.grbMemberInfor.Controls.Add(this.label5);
            this.grbMemberInfor.Controls.Add(this.dateTimePickerNgayKetThuc);
            this.grbMemberInfor.Controls.Add(this.dateTimePickerNgayDangKi);
            this.grbMemberInfor.Controls.Add(this.label4);
            this.grbMemberInfor.Controls.Add(this.label3);
            this.grbMemberInfor.Controls.Add(this.txtSDTKH);
            this.grbMemberInfor.Controls.Add(this.label1);
            this.grbMemberInfor.Controls.Add(this.txtDiaChiKH);
            this.grbMemberInfor.Controls.Add(this.label2);
            this.grbMemberInfor.Controls.Add(this.cbxGioiTinhKH);
            this.grbMemberInfor.Controls.Add(this.lblSex);
            this.grbMemberInfor.Controls.Add(this.txtHoTenKH);
            this.grbMemberInfor.Controls.Add(this.lblFullName);
            this.grbMemberInfor.Location = new System.Drawing.Point(12, 361);
            this.grbMemberInfor.Name = "grbMemberInfor";
            this.grbMemberInfor.Size = new System.Drawing.Size(700, 190);
            this.grbMemberInfor.TabIndex = 6;
            this.grbMemberInfor.TabStop = false;
            this.grbMemberInfor.Text = "Thông tin khách hàng";
            // 
            // txtGoiTap
            // 
            this.txtGoiTap.Enabled = false;
            this.txtGoiTap.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoiTap.Location = new System.Drawing.Point(518, 108);
            this.txtGoiTap.Name = "txtGoiTap";
            this.txtGoiTap.ReadOnly = true;
            this.txtGoiTap.Size = new System.Drawing.Size(151, 26);
            this.txtGoiTap.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(452, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 18);
            this.label6.TabIndex = 22;
            this.label6.Text = "Gói tập:";
            // 
            // txtHLV
            // 
            this.txtHLV.Enabled = false;
            this.txtHLV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHLV.Location = new System.Drawing.Point(518, 147);
            this.txtHLV.Name = "txtHLV";
            this.txtHLV.ReadOnly = true;
            this.txtHLV.Size = new System.Drawing.Size(151, 26);
            this.txtHLV.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(472, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 18);
            this.label5.TabIndex = 20;
            this.label5.Text = "HLV:";
            // 
            // dateTimePickerNgayKetThuc
            // 
            this.dateTimePickerNgayKetThuc.Enabled = false;
            this.dateTimePickerNgayKetThuc.Location = new System.Drawing.Point(125, 146);
            this.dateTimePickerNgayKetThuc.Name = "dateTimePickerNgayKetThuc";
            this.dateTimePickerNgayKetThuc.Size = new System.Drawing.Size(289, 24);
            this.dateTimePickerNgayKetThuc.TabIndex = 19;
            // 
            // dateTimePickerNgayDangKi
            // 
            this.dateTimePickerNgayDangKi.Enabled = false;
            this.dateTimePickerNgayDangKi.Location = new System.Drawing.Point(125, 108);
            this.dateTimePickerNgayDangKi.Name = "dateTimePickerNgayDangKi";
            this.dateTimePickerNgayDangKi.Size = new System.Drawing.Size(289, 24);
            this.dateTimePickerNgayDangKi.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(17, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 18);
            this.label4.TabIndex = 16;
            this.label4.Text = "Ngày kết thúc:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(18, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 18);
            this.label3.TabIndex = 14;
            this.label3.Text = "Ngày đăng ký:";
            // 
            // txtSDTKH
            // 
            this.txtSDTKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSDTKH.Location = new System.Drawing.Point(518, 67);
            this.txtSDTKH.Name = "txtSDTKH";
            this.txtSDTKH.Size = new System.Drawing.Size(151, 26);
            this.txtSDTKH.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(470, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "SĐT:";
            // 
            // txtDiaChiKH
            // 
            this.txtDiaChiKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiKH.Location = new System.Drawing.Point(80, 67);
            this.txtDiaChiKH.Name = "txtDiaChiKH";
            this.txtDiaChiKH.Size = new System.Drawing.Size(334, 26);
            this.txtDiaChiKH.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "Địa chỉ:";
            // 
            // cbxGioiTinhKH
            // 
            this.cbxGioiTinhKH.FormattingEnabled = true;
            this.cbxGioiTinhKH.Items.AddRange(new object[] {
            "Nam",
            "Nữ"});
            this.cbxGioiTinhKH.Location = new System.Drawing.Point(518, 27);
            this.cbxGioiTinhKH.Name = "cbxGioiTinhKH";
            this.cbxGioiTinhKH.Size = new System.Drawing.Size(79, 26);
            this.cbxGioiTinhKH.TabIndex = 9;
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Location = new System.Drawing.Point(446, 30);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(66, 18);
            this.lblSex.TabIndex = 8;
            this.lblSex.Text = "Giới tính:";
            // 
            // txtHoTenKH
            // 
            this.txtHoTenKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTenKH.Location = new System.Drawing.Point(80, 26);
            this.txtHoTenKH.Name = "txtHoTenKH";
            this.txtHoTenKH.Size = new System.Drawing.Size(334, 26);
            this.txtHoTenKH.TabIndex = 7;
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = true;
            this.lblFullName.Location = new System.Drawing.Point(18, 30);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(56, 18);
            this.lblFullName.TabIndex = 0;
            this.lblFullName.Text = "Họ tên:";
            // 
            // btnGiaHan
            // 
            this.btnGiaHan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiaHan.Location = new System.Drawing.Point(746, 382);
            this.btnGiaHan.Name = "btnGiaHan";
            this.btnGiaHan.Size = new System.Drawing.Size(134, 65);
            this.btnGiaHan.TabIndex = 7;
            this.btnGiaHan.Text = "Gia hạn";
            this.btnGiaHan.UseVisualStyleBackColor = true;
            this.btnGiaHan.Click += new System.EventHandler(this.btnGiaHan_Click);
            // 
            // btnLuuCS
            // 
            this.btnLuuCS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuCS.Location = new System.Drawing.Point(746, 466);
            this.btnLuuCS.Name = "btnLuuCS";
            this.btnLuuCS.Size = new System.Drawing.Size(134, 69);
            this.btnLuuCS.TabIndex = 9;
            this.btnLuuCS.Text = "Lưu chỉnh sửa thông tin";
            this.btnLuuCS.UseVisualStyleBackColor = true;
            this.btnLuuCS.Click += new System.EventHandler(this.btnLuuCS_Click);
            // 
            // fMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 564);
            this.Controls.Add(this.btnLuuCS);
            this.Controls.Add(this.btnGiaHan);
            this.Controls.Add(this.grbMemberInfor);
            this.Controls.Add(this.txtTraCuuKH);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblSearchMember);
            this.Controls.Add(this.grbMemberList);
            this.Controls.Add(this.lblForm);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "fMember";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tài khoản thành viên";
            this.Load += new System.EventHandler(this.fMember_Load);
            this.grbMemberList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDanhSachThanhVien)).EndInit();
            this.grbMemberInfor.ResumeLayout(false);
            this.grbMemberInfor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblForm;
        private System.Windows.Forms.GroupBox grbMemberList;
        private System.Windows.Forms.DataGridView dataGridViewDanhSachThanhVien;
        private System.Windows.Forms.Label lblSearchMember;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtTraCuuKH;
        private System.Windows.Forms.GroupBox grbMemberInfor;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.TextBox txtHoTenKH;
        private System.Windows.Forms.Label lblFullName;
        private System.Windows.Forms.ComboBox cbxGioiTinhKH;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox txtHLV;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePickerNgayKetThuc;
        private System.Windows.Forms.DateTimePicker dateTimePickerNgayDangKi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSDTKH;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDiaChiKH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGiaHan;
        private System.Windows.Forms.Button btnLuuCS;
        private System.Windows.Forms.TextBox txtGoiTap;
        private System.Windows.Forms.Label label6;
    }
}