﻿namespace _1461589_1660215_1660218_1660259_1660290
{
    partial class fManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStripUDQLPG = new System.Windows.Forms.MenuStrip();
            this.adminToolStripMenuAdmin = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLíLễTânToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLíHuấnLuyệnViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLíPhòngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLíGóiTậpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLíThiếtBịToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLíStoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLíBánHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLíLịchHLVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêDoanhThuChiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thôngKêLươngNhânViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêLợiNhuậnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolStripMenuAccountInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.đổiMậtKhẩuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.đổiThôngTinCáNhânToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trợGiúpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thoátToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblApp = new System.Windows.Forms.Label();
            this.btnSignUp = new System.Windows.Forms.Button();
            this.btnMember = new System.Windows.Forms.Button();
            this.btnDay = new System.Windows.Forms.Button();
            this.btnStore = new System.Windows.Forms.Button();
            this.menuStripUDQLPG.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 33);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1413, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuStripUDQLPG
            // 
            this.menuStripUDQLPG.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.menuStripUDQLPG.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adminToolStripMenuAdmin,
            this.adminToolStripMenuAccountInfo,
            this.trợGiúpToolStripMenuItem,
            this.thoátToolStripMenuItem});
            this.menuStripUDQLPG.Location = new System.Drawing.Point(0, 0);
            this.menuStripUDQLPG.Name = "menuStripUDQLPG";
            this.menuStripUDQLPG.Size = new System.Drawing.Size(1413, 33);
            this.menuStripUDQLPG.TabIndex = 1;
            this.menuStripUDQLPG.Text = "menuStripUDQLPG";
            // 
            // adminToolStripMenuAdmin
            // 
            this.adminToolStripMenuAdmin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quảnLíLễTânToolStripMenuItem,
            this.quảnLíHuấnLuyệnViênToolStripMenuItem,
            this.quảnLíPhòngToolStripMenuItem,
            this.thốngKêToolStripMenuItem});
            this.adminToolStripMenuAdmin.Name = "adminToolStripMenuAdmin";
            this.adminToolStripMenuAdmin.Size = new System.Drawing.Size(79, 29);
            this.adminToolStripMenuAdmin.Text = "Admin";
            // 
            // quảnLíLễTânToolStripMenuItem
            // 
            this.quảnLíLễTânToolStripMenuItem.Name = "quảnLíLễTânToolStripMenuItem";
            this.quảnLíLễTânToolStripMenuItem.Size = new System.Drawing.Size(284, 30);
            this.quảnLíLễTânToolStripMenuItem.Text = "Quản lí lễ tân";
            this.quảnLíLễTânToolStripMenuItem.Click += new System.EventHandler(this.quảnLíLễTânToolStripMenuItem_Click);
            // 
            // quảnLíHuấnLuyệnViênToolStripMenuItem
            // 
            this.quảnLíHuấnLuyệnViênToolStripMenuItem.Name = "quảnLíHuấnLuyệnViênToolStripMenuItem";
            this.quảnLíHuấnLuyệnViênToolStripMenuItem.Size = new System.Drawing.Size(284, 30);
            this.quảnLíHuấnLuyệnViênToolStripMenuItem.Text = "Quản lí huấn luyện viên";
            this.quảnLíHuấnLuyệnViênToolStripMenuItem.Click += new System.EventHandler(this.quảnLíHuấnLuyệnViênToolStripMenuItem_Click);
            // 
            // quảnLíPhòngToolStripMenuItem
            // 
            this.quảnLíPhòngToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quảnLíGóiTậpToolStripMenuItem,
            this.quảnLíThiếtBịToolStripMenuItem,
            this.quảnLíStoreToolStripMenuItem,
            this.quảnLíBánHàngToolStripMenuItem,
            this.quảnLíLịchHLVToolStripMenuItem});
            this.quảnLíPhòngToolStripMenuItem.Name = "quảnLíPhòngToolStripMenuItem";
            this.quảnLíPhòngToolStripMenuItem.Size = new System.Drawing.Size(284, 30);
            this.quảnLíPhòngToolStripMenuItem.Text = "Quản lí phòng";
            // 
            // quảnLíGóiTậpToolStripMenuItem
            // 
            this.quảnLíGóiTậpToolStripMenuItem.Name = "quảnLíGóiTậpToolStripMenuItem";
            this.quảnLíGóiTậpToolStripMenuItem.Size = new System.Drawing.Size(231, 30);
            this.quảnLíGóiTậpToolStripMenuItem.Text = "Quản lí gói tập";
            this.quảnLíGóiTậpToolStripMenuItem.Click += new System.EventHandler(this.quảnLíGóiTậpToolStripMenuItem_Click);
            // 
            // quảnLíThiếtBịToolStripMenuItem
            // 
            this.quảnLíThiếtBịToolStripMenuItem.Name = "quảnLíThiếtBịToolStripMenuItem";
            this.quảnLíThiếtBịToolStripMenuItem.Size = new System.Drawing.Size(231, 30);
            this.quảnLíThiếtBịToolStripMenuItem.Text = "Quản lí thiết bị";
            this.quảnLíThiếtBịToolStripMenuItem.Click += new System.EventHandler(this.quảnLíThiếtBịToolStripMenuItem_Click);
            // 
            // quảnLíStoreToolStripMenuItem
            // 
            this.quảnLíStoreToolStripMenuItem.Name = "quảnLíStoreToolStripMenuItem";
            this.quảnLíStoreToolStripMenuItem.Size = new System.Drawing.Size(220, 30);
            this.quảnLíStoreToolStripMenuItem.Text = "Quản lí Store";
            this.quảnLíStoreToolStripMenuItem.Click += new System.EventHandler(this.quảnLíStoreToolStripMenuItem_Click);
            // 
            // quảnLíBánHàngToolStripMenuItem
            // 
            this.quảnLíBánHàngToolStripMenuItem.Name = "quảnLíBánHàngToolStripMenuItem";
            this.quảnLíBánHàngToolStripMenuItem.Size = new System.Drawing.Size(220, 30);
            this.quảnLíBánHàngToolStripMenuItem.Text = "Quản lí hóa đơn";
            this.quảnLíBánHàngToolStripMenuItem.Click += new System.EventHandler(this.quảnLíBánHàngToolStripMenuItem_Click);
            // 
            // quảnLíLịchHLVToolStripMenuItem
            // 
            this.quảnLíLịchHLVToolStripMenuItem.Name = "quảnLíLịchHLVToolStripMenuItem";
            this.quảnLíLịchHLVToolStripMenuItem.Size = new System.Drawing.Size(231, 30);
            this.quảnLíLịchHLVToolStripMenuItem.Text = "Quản lí lịch HLV";
            this.quảnLíLịchHLVToolStripMenuItem.Click += new System.EventHandler(this.quảnLíLịchHLVToolStripMenuItem_Click);
            // 
            // thốngKêToolStripMenuItem
            // 
            this.thốngKêToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thốngKêDoanhThuChiToolStripMenuItem,
            this.thôngKêLươngNhânViênToolStripMenuItem,
            this.thốngKêLợiNhuậnToolStripMenuItem});
            this.thốngKêToolStripMenuItem.Name = "thốngKêToolStripMenuItem";
            this.thốngKêToolStripMenuItem.Size = new System.Drawing.Size(284, 30);
            this.thốngKêToolStripMenuItem.Text = "Thống kê";
            // 
            // thốngKêDoanhThuChiToolStripMenuItem
            // 
            this.thốngKêDoanhThuChiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thuToolStripMenuItem,
            this.chiToolStripMenuItem});
            this.thốngKêDoanhThuChiToolStripMenuItem.Name = "thốngKêDoanhThuChiToolStripMenuItem";
            this.thốngKêDoanhThuChiToolStripMenuItem.Size = new System.Drawing.Size(304, 30);
            this.thốngKêDoanhThuChiToolStripMenuItem.Text = "Thống kê doanh thu, chi";
            // 
            // thuToolStripMenuItem
            // 
            this.thuToolStripMenuItem.Name = "thuToolStripMenuItem";
            this.thuToolStripMenuItem.Size = new System.Drawing.Size(180, 30);
            this.thuToolStripMenuItem.Text = "Thu";
            this.thuToolStripMenuItem.Click += new System.EventHandler(this.thuToolStripMenuItem_Click);
            // 
            // chiToolStripMenuItem
            // 
            this.chiToolStripMenuItem.Name = "chiToolStripMenuItem";
            this.chiToolStripMenuItem.Size = new System.Drawing.Size(180, 30);
            this.chiToolStripMenuItem.Text = "Chi";
            this.chiToolStripMenuItem.Click += new System.EventHandler(this.chiToolStripMenuItem_Click);
            // 
            // thôngKêLươngNhânViênToolStripMenuItem
            // 
            this.thôngKêLươngNhânViênToolStripMenuItem.Name = "thôngKêLươngNhânViênToolStripMenuItem";
            this.thôngKêLươngNhânViênToolStripMenuItem.Size = new System.Drawing.Size(304, 30);
            this.thôngKêLươngNhânViênToolStripMenuItem.Text = "Thống kê lương nhân viên";
            this.thôngKêLươngNhânViênToolStripMenuItem.Click += new System.EventHandler(this.thôngKêLươngNhânViênToolStripMenuItem_Click);
            // 
            // thốngKêLợiNhuậnToolStripMenuItem
            // 
            this.thốngKêLợiNhuậnToolStripMenuItem.Name = "thốngKêLợiNhuậnToolStripMenuItem";
            this.thốngKêLợiNhuậnToolStripMenuItem.Size = new System.Drawing.Size(304, 30);
            this.thốngKêLợiNhuậnToolStripMenuItem.Text = "Thống kê lợi nhuận";
            this.thốngKêLợiNhuậnToolStripMenuItem.Click += new System.EventHandler(this.thốngKêLợiNhuậnToolStripMenuItem_Click);
            // 
            // adminToolStripMenuAccountInfo
            // 
            this.adminToolStripMenuAccountInfo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.đổiMậtKhẩuToolStripMenuItem,
            this.đổiThôngTinCáNhânToolStripMenuItem});
            this.adminToolStripMenuAccountInfo.Name = "adminToolStripMenuAccountInfo";
            this.adminToolStripMenuAccountInfo.Size = new System.Drawing.Size(284, 29);
            this.adminToolStripMenuAccountInfo.Text = "Thông tin tài khoản đăng nhập";
            // 
            // đổiMậtKhẩuToolStripMenuItem
            // 
            this.đổiMậtKhẩuToolStripMenuItem.Name = "đổiMậtKhẩuToolStripMenuItem";
            this.đổiMậtKhẩuToolStripMenuItem.Size = new System.Drawing.Size(267, 30);
            this.đổiMậtKhẩuToolStripMenuItem.Text = "Đổi mật khẩu";
            this.đổiMậtKhẩuToolStripMenuItem.Click += new System.EventHandler(this.đổiMậtKhẩuToolStripMenuItem_Click);
            // 
            // đổiThôngTinCáNhânToolStripMenuItem
            // 
            this.đổiThôngTinCáNhânToolStripMenuItem.Name = "đổiThôngTinCáNhânToolStripMenuItem";
            this.đổiThôngTinCáNhânToolStripMenuItem.Size = new System.Drawing.Size(267, 30);
            this.đổiThôngTinCáNhânToolStripMenuItem.Text = "Đổi thông tin cá nhân";
            this.đổiThôngTinCáNhânToolStripMenuItem.Click += new System.EventHandler(this.đổiThôngTinCáNhânToolStripMenuItem_Click);
            // 
            // trợGiúpToolStripMenuItem
            // 
            this.trợGiúpToolStripMenuItem.Name = "trợGiúpToolStripMenuItem";
            this.trợGiúpToolStripMenuItem.Size = new System.Drawing.Size(93, 29);
            this.trợGiúpToolStripMenuItem.Text = "Trợ giúp";
            this.trợGiúpToolStripMenuItem.Click += new System.EventHandler(this.trợGiúpToolStripMenuItem_Click);
            // 
            // thoátToolStripMenuItem
            // 
            this.thoátToolStripMenuItem.Name = "thoátToolStripMenuItem";
            this.thoátToolStripMenuItem.Size = new System.Drawing.Size(72, 29);
            this.thoátToolStripMenuItem.Text = "Thoát";
            this.thoátToolStripMenuItem.Click += new System.EventHandler(this.thoátToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblApp);
            this.panel1.Location = new System.Drawing.Point(12, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1388, 72);
            this.panel1.TabIndex = 2;
            // 
            // lblApp
            // 
            this.lblApp.AutoSize = true;
            this.lblApp.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApp.ForeColor = System.Drawing.Color.Crimson;
            this.lblApp.Location = new System.Drawing.Point(213, 9);
            this.lblApp.Name = "lblApp";
            this.lblApp.Size = new System.Drawing.Size(965, 55);
            this.lblApp.TabIndex = 0;
            this.lblApp.Text = "Ứng dụng quản lý phòng tập Fitness and Yoga";
            this.lblApp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSignUp
            // 
            this.btnSignUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSignUp.Location = new System.Drawing.Point(139, 143);
            this.btnSignUp.Name = "btnSignUp";
            this.btnSignUp.Size = new System.Drawing.Size(200, 381);
            this.btnSignUp.TabIndex = 3;
            this.btnSignUp.Text = "Đăng ký tài khoản thành viên";
            this.btnSignUp.UseVisualStyleBackColor = true;
            this.btnSignUp.Click += new System.EventHandler(this.btnSignUp_Click);
            // 
            // btnMember
            // 
            this.btnMember.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMember.Location = new System.Drawing.Point(445, 143);
            this.btnMember.Name = "btnMember";
            this.btnMember.Size = new System.Drawing.Size(200, 381);
            this.btnMember.TabIndex = 4;
            this.btnMember.Text = "Tài khoản thành viên";
            this.btnMember.UseVisualStyleBackColor = true;
            this.btnMember.Click += new System.EventHandler(this.btnMember_Click);
            // 
            // btnDay
            // 
            this.btnDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDay.Location = new System.Drawing.Point(758, 143);
            this.btnDay.Name = "btnDay";
            this.btnDay.Size = new System.Drawing.Size(200, 381);
            this.btnDay.TabIndex = 5;
            this.btnDay.Text = "Tập ngày";
            this.btnDay.UseVisualStyleBackColor = true;
            this.btnDay.Click += new System.EventHandler(this.btnDay_Click);
            // 
            // btnStore
            // 
            this.btnStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStore.Location = new System.Drawing.Point(1069, 143);
            this.btnStore.Name = "btnStore";
            this.btnStore.Size = new System.Drawing.Size(200, 381);
            this.btnStore.TabIndex = 6;
            this.btnStore.Text = "Store";
            this.btnStore.UseVisualStyleBackColor = true;
            this.btnStore.Click += new System.EventHandler(this.btnStore_Click);
            // 
            // fManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1413, 629);
            this.Controls.Add(this.btnStore);
            this.Controls.Add(this.btnDay);
            this.Controls.Add(this.btnMember);
            this.Controls.Add(this.btnSignUp);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStripUDQLPG);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "fManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Ứng dụng quản lí phòng gym - nhóm 01";
            this.Load += new System.EventHandler(this.fManage_Load_1);
            this.menuStripUDQLPG.ResumeLayout(false);
            this.menuStripUDQLPG.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.MenuStrip menuStripUDQLPG;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuAdmin;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuAccountInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblApp;
        private System.Windows.Forms.ToolStripMenuItem thoátToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLíLễTânToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLíHuấnLuyệnViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLíPhòngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem đổiMậtKhẩuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem đổiThôngTinCáNhânToolStripMenuItem;
        private System.Windows.Forms.Button btnSignUp;
        private System.Windows.Forms.Button btnMember;
        private System.Windows.Forms.Button btnDay;
        private System.Windows.Forms.Button btnStore;
        private System.Windows.Forms.ToolStripMenuItem quảnLíBánHàngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLíGóiTậpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trợGiúpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLíThiếtBịToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLíStoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêDoanhThuChiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thôngKêLươngNhânViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêLợiNhuậnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLíLịchHLVToolStripMenuItem;
    }
}