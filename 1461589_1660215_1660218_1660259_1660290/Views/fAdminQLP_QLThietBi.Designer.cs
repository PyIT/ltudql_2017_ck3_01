﻿namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    partial class fAdminQLP_QLThietBi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnSua = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.grbThietBi = new System.Windows.Forms.GroupBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.MaGoi = new System.Windows.Forms.Label();
            this.txtSoLuong = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenThietBi = new System.Windows.Forms.TextBox();
            this.lblFullName = new System.Windows.Forms.Label();
            this.grbList = new System.Windows.Forms.GroupBox();
            this.dataGridViewDSThietBi = new System.Windows.Forms.DataGridView();
            this.lblForm = new System.Windows.Forms.Label();
            this.grbThietBi.SuspendLayout();
            this.grbList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDSThietBi)).BeginInit();
            this.SuspendLayout();
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Location = new System.Drawing.Point(43, 389);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(134, 42);
            this.btnXoa.TabIndex = 23;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnSua
            // 
            this.btnSua.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Location = new System.Drawing.Point(223, 389);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(134, 42);
            this.btnSua.TabIndex = 22;
            this.btnSua.Text = "Sửa";
            this.btnSua.UseVisualStyleBackColor = true;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnThem
            // 
            this.btnThem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Location = new System.Drawing.Point(451, 286);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(134, 42);
            this.btnThem.TabIndex = 21;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // grbThietBi
            // 
            this.grbThietBi.Controls.Add(this.txtID);
            this.grbThietBi.Controls.Add(this.MaGoi);
            this.grbThietBi.Controls.Add(this.txtSoLuong);
            this.grbThietBi.Controls.Add(this.label2);
            this.grbThietBi.Controls.Add(this.txtTenThietBi);
            this.grbThietBi.Controls.Add(this.lblFullName);
            this.grbThietBi.Location = new System.Drawing.Point(21, 247);
            this.grbThietBi.Name = "grbThietBi";
            this.grbThietBi.Size = new System.Drawing.Size(379, 116);
            this.grbThietBi.TabIndex = 20;
            this.grbThietBi.TabStop = false;
            this.grbThietBi.Text = "Thông tin thiết bị";
            // 
            // txtID
            // 
            this.txtID.Enabled = false;
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Location = new System.Drawing.Point(303, 67);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(52, 26);
            this.txtID.TabIndex = 15;
            // 
            // MaGoi
            // 
            this.MaGoi.AutoSize = true;
            this.MaGoi.Location = new System.Drawing.Point(262, 70);
            this.MaGoi.Name = "MaGoi";
            this.MaGoi.Size = new System.Drawing.Size(35, 20);
            this.MaGoi.TabIndex = 14;
            this.MaGoi.Text = "Mã:";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.Location = new System.Drawing.Point(89, 67);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(149, 26);
            this.txtSoLuong.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Số lượng:";
            // 
            // txtTenThietBi
            // 
            this.txtTenThietBi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenThietBi.Location = new System.Drawing.Point(89, 26);
            this.txtTenThietBi.Name = "txtTenThietBi";
            this.txtTenThietBi.Size = new System.Drawing.Size(266, 26);
            this.txtTenThietBi.TabIndex = 7;
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = true;
            this.lblFullName.Location = new System.Drawing.Point(43, 29);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(40, 20);
            this.lblFullName.TabIndex = 0;
            this.lblFullName.Text = "Tên:";
            // 
            // grbList
            // 
            this.grbList.Controls.Add(this.dataGridViewDSThietBi);
            this.grbList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbList.Location = new System.Drawing.Point(21, 43);
            this.grbList.Name = "grbList";
            this.grbList.Size = new System.Drawing.Size(596, 198);
            this.grbList.TabIndex = 19;
            this.grbList.TabStop = false;
            this.grbList.Text = "Danh sách";
            // 
            // dataGridViewDSThietBi
            // 
            this.dataGridViewDSThietBi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDSThietBi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDSThietBi.Location = new System.Drawing.Point(6, 23);
            this.dataGridViewDSThietBi.Name = "dataGridViewDSThietBi";
            this.dataGridViewDSThietBi.ReadOnly = true;
            this.dataGridViewDSThietBi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDSThietBi.Size = new System.Drawing.Size(582, 169);
            this.dataGridViewDSThietBi.TabIndex = 0;
            this.dataGridViewDSThietBi.SelectionChanged += new System.EventHandler(this.dataGridViewDSThietBi_SelectionChanged);
            // 
            // lblForm
            // 
            this.lblForm.AutoSize = true;
            this.lblForm.Font = new System.Drawing.Font("Times New Roman", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForm.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblForm.Location = new System.Drawing.Point(196, 7);
            this.lblForm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForm.Name = "lblForm";
            this.lblForm.Size = new System.Drawing.Size(251, 35);
            this.lblForm.TabIndex = 18;
            this.lblForm.Text = "Thiết bị phòng tập";
            // 
            // fAdminQLP_QLThietBi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 449);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.grbThietBi);
            this.Controls.Add(this.grbList);
            this.Controls.Add(this.lblForm);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.MaximizeBox = false;
            this.Name = "fAdminQLP_QLThietBi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lí thiết bị";
            this.Load += new System.EventHandler(this.fAdminQLP_QLThietBi_Load);
            this.grbThietBi.ResumeLayout(false);
            this.grbThietBi.PerformLayout();
            this.grbList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDSThietBi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.GroupBox grbThietBi;
        private System.Windows.Forms.TextBox txtSoLuong;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTenThietBi;
        private System.Windows.Forms.Label lblFullName;
        private System.Windows.Forms.GroupBox grbList;
        private System.Windows.Forms.DataGridView dataGridViewDSThietBi;
        private System.Windows.Forms.Label lblForm;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label MaGoi;
    }
}