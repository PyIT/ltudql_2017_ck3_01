﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _1461589_1660215_1660218_1660259_1660290.DAO;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fDay : Form
    {
        // Check string all num
        bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        public fDay()
        {
            InitializeComponent();
        }

        private void fDay_Load(object sender, EventArgs e)
        {
            cb_BM.DataSource = BoMonDAO.Instance.LayDSBoMon();
            cb_BM.DisplayMember = "TenBM";
            
            txt_TT.Text = "0";
        }

        private void btnInHoaDon_Click(object sender, EventArgs e)
        {
            string ChiTiet = "";
            ChiTiet += "Ten KH: " + txt_Ten.Text + " - " + "SDT: " + txt_SDT.Text + " - " + "Ngay: " + datetime_picker.Value.ToString();
            if (txt_TT.Text != "0")
            {
                if (IsNumber(txt_SDT.Text))
                {
                    if (HoaDonDAO.Instance.ThemHoaDon(txt_TT.Text, ChiTiet))
                    {
                      //  DAO.HoaDonDAO.Instance.ThemGoiTap(cb_BM.Text, txt_SDT.Text);
                        MessageBox.Show("In hóa đơn thành công, cảm ơn quý khách!", "Thành công", MessageBoxButtons.OK);

                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Số điện thoại chỉ được chứa ký tự chữ số!", "UI, có gì đó không đúng", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Kiểm tra lại, không được để trống thông tin! HOẶC vui lòng chọn lại bộ môn nếu tổng tiền đang là 0!", "UI, có gì đó không đúng", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cb_BM_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable tb = BoMonDAO.Instance.LayGiaBM(cb_BM.Text);
            double Gia = 0;
            foreach (DataRow r in tb.Rows)
            {
                Gia = Convert.ToDouble(r["GiaBM"]) / 10;
            }
            txt_TT.Text = Gia.ToString();
        }
    }
}
