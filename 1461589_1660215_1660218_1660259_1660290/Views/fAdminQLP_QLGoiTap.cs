﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _1461589_1660215_1660218_1660259_1660290.DAO;
using _1461589_1660215_1660218_1660259_1660290.DTO;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fAdminQLP_QLGoiTap : Form
    {
        public fAdminQLP_QLGoiTap()
        {
            InitializeComponent();
        }

        private void fAdminQLP_QLGoiTap_Load(object sender, EventArgs e)
        {
            dataGridViewDSGoiTap.DataSource = DAO.QuanLiGoiTapDAO.Instance.LoadDanhSachGoiTap();
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtID.Text == "")
            {
                if (txtTenGoiTap.Text != "" && txtGiaTien.Text != "")
                {

                    if (IsNumber(txtGiaTien.Text))
                    {
                        float GiaTien = float.Parse(txtGiaTien.Text);
                        if (QuanLiGoiTapDAO.Instance.ThemGoiTap(txtTenGoiTap.Text, GiaTien))
                        {
                            MessageBox.Show("Thêm Gói Tập thành công !", "Thành công", MessageBoxButtons.OK);
                            dataGridViewDSGoiTap.DataSource = DAO.QuanLiGoiTapDAO.Instance.LoadDanhSachGoiTap();
                            txtTenGoiTap.Clear();
                            txtGiaTien.Clear();
                            txtID.Clear();

                        }
                        else MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    else
                    {
                        MessageBox.Show("Kiểm Tra Lại Giá Tiền!");
                    }
                }
                else
                {
                    MessageBox.Show("Vui Lòng Kiểm Tra Lại Thông Tin");
                }
            }
            else
                MessageBox.Show("Nhấn vào trường dữ liệu trống để xóa mã trước khi thêm!");
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (DAO.QuanLiGoiTapDAO.Instance.XoaGoiTap(txtID.Text))
            {
                if (txtGiaTien.Text != "" && txtTenGoiTap.Text != "")
                {
                    MessageBox.Show("Xóa Thành Công!");
                    dataGridViewDSGoiTap.DataSource = DAO.QuanLiGoiTapDAO.Instance.LoadDanhSachGoiTap();
                    txtTenGoiTap.Clear();
                    txtGiaTien.Clear();
                    txtID.Clear();

                }
                else
                    MessageBox.Show("Chọn Bộ Môn Xóa");
            }
            else
                MessageBox.Show("Không Thể Xóa");
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (txtID.Text != "")
            {
                if (txtTenGoiTap.Text != "" && txtTenGoiTap.Text != "")
                {
                    if (IsNumber(txtGiaTien.Text))
                    {
                        float GiaTien = float.Parse(txtGiaTien.Text);
                        if (DAO.QuanLiGoiTapDAO.Instance.SuaThongTinGoiTap(txtTenGoiTap.Text, GiaTien, txtID.Text))
                        {
                            MessageBox.Show("Sửa Thành Công!");
                            dataGridViewDSGoiTap.DataSource = DAO.QuanLiGoiTapDAO.Instance.LoadDanhSachGoiTap();
                            txtTenGoiTap.Clear();
                            txtGiaTien.Clear();
                            txtID.Clear();

                        }
                    }
                    else
                        MessageBox.Show("Kiểm Tra Lại Giá Tiền");

                }
                else
                    MessageBox.Show("Bạn Cần Nhập Đầy Đủ Thông Tin");
            }
            else
            MessageBox.Show("ID Không Hợp Lệ");
        }
        
        private void dataGridViewDSGoiTap_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int idex = dataGridViewDSGoiTap.CurrentRow.Index;
                txtID.Text = dataGridViewDSGoiTap.Rows[idex].Cells[0].Value.ToString();
                txtTenGoiTap.Text = dataGridViewDSGoiTap.Rows[idex].Cells[1].Value.ToString();
                txtGiaTien.Text = dataGridViewDSGoiTap.Rows[idex].Cells[2].Value.ToString();

            }
            catch
            {
                MessageBox.Show("Không thể click chọn ở đây!", "Ui, có gì đó không đúng!", MessageBoxButtons.OK);
            }
        }
    }
}
