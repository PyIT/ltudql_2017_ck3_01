﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fStore : Form
    {
        public fStore()
        {
            InitializeComponent();
        }

        private void lblForm_Click(object sender, EventArgs e)
        {

        }

        private void fStore_Load(object sender, EventArgs e)
        {
            // SET value comboBox SanPham
            cbSP.DataSource = SanPhamDAO.Instance.LayDSTenSP();
            cbSP.DisplayMember = "TenSP";

            txtTT.Text = "0";
            txtSL.Text = "1";

            dataGridViewDSChon.Columns.Add("TenSP","Tên sản phẩm");
            dataGridViewDSChon.Columns.Add("GiaSP", "Giá sản phẩm");
            dataGridViewDSChon.Columns.Add("SoLuongSP", "Số lượng sản phẩm");
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtGia.Text != "" && cbSP.Text != "" && txtSL.Text != "") {
                dataGridViewDSChon.Rows.Add(cbSP.Text, txtGia.Text, txtSL.Text);

                double Tien = Convert.ToDouble(txtTT.Text) + (Convert.ToDouble(txtGia.Text) * Convert.ToDouble(txtSL.Text));
                txtTT.Text = Tien.ToString();
                txtSL.Text = "1";
            }
            else
            {
                MessageBox.Show("Cần nhập đầy đủ thông tin", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int index = dataGridViewDSChon.CurrentRow.Index;

                double Tien = Convert.ToDouble(txtTT.Text) - (Convert.ToDouble(dataGridViewDSChon.Rows[index].Cells[1].Value.ToString()) * Convert.ToDouble(dataGridViewDSChon.Rows[index].Cells[2].Value.ToString()));
                txtTT.Text = Tien.ToString();

                dataGridViewDSChon.Rows.Remove(dataGridViewDSChon.CurrentRow);
            }
            catch
            {
                MessageBox.Show("Không có dòng để xóa", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnInHoaDon_Click(object sender, EventArgs e)
        {
            string ChiTiet = "";
            int i = 1;
            foreach(DataGridViewRow row in dataGridViewDSChon.Rows)
            {
                if (i == dataGridViewDSChon.RowCount) break;
                ChiTiet += "TenSP: ";
                ChiTiet += row.Cells["TenSP"].Value.ToString();
                ChiTiet += " - Gia: ";
                ChiTiet += row.Cells["GiaSP"].Value.ToString();
                ChiTiet += " - SoLuong: ";
                ChiTiet += row.Cells["SoLuongSP"].Value.ToString();
                ChiTiet += " ------ ";
                i++;
            }
            if (ChiTiet != "") {
                if (HoaDonStoreDAO.Instance.ThemHoaDonStore(txtTT.Text, ChiTiet))
                {
                    MessageBox.Show("In hóa đơn thành công, cảm ơn quý khách!", "Thành công", MessageBoxButtons.OK);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Không có sản phẩm", "UI, có gì đó không đúng", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cbSP_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSL.Text = "1";
            DataTable tb = SanPhamDAO.Instance.LayGiaSP(cbSP.Text);
            foreach( DataRow r in tb.Rows){
                txtGia.Text = r["GiaSP"].ToString();
            }
        }
    }
}
