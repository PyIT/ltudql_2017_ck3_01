﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using _1461589_1660215_1660218_1660259_1660290.DAO;
using _1461589_1660215_1660218_1660259_1660290.DTO;


namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fSignUp : Form
    {
        int GioiTinh;
        public fSignUp()
        {
            InitializeComponent();

        }

        private void fSignUp_Load(object sender, EventArgs e)
        {
            GioiTinh = 1;
        }
        
        private void radioButtonNam_CheckedChanged(object sender, EventArgs e)
        {
            GioiTinh = 1;
        }

        private void radioButtonNu_CheckedChanged(object sender, EventArgs e)
        {
            GioiTinh = 0;
        }
        private void btnChup_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnAccSignUp_Click(object sender, EventArgs e)
        {
            if (txtHoTen.Text != "" && txtDiaChi.Text != "" && txtSDT.Text != "")
            {
                var NgayDangKy = DateTime.Now;
                var NgayDangKy1 = NgayDangKy.ToShortDateString();
                var NgayKetThuc = NgayDangKy1;
                var NgaySinh = dateTimePickerNgaySinh.Value;
                var NgaySinh1 = NgaySinh.ToShortDateString();
                TimeSpan Ngay = DateTime.Parse(NgayDangKy1).Subtract(DateTime.Parse(NgaySinh1));
                string Ngay1 = Ngay.TotalDays.ToString();
                int Ngay2 = int.Parse(Ngay1) / 365;
                if (IsNumber(txtSDT.Text))
                {
                    if (Ngay2 <= 65 && Ngay2 >= 12)
                    {
                        if (SignUpDAO.Instance.DangKiThanhVien(txtHoTen.Text, GioiTinh, txtDiaChi.Text, txtSDT.Text, NgaySinh1, NgayDangKy1, NgayKetThuc))
                        {
                            MessageBox.Show("Thêm Tài Khoản thành công !", "Thành công", MessageBoxButtons.OK);
                            txtHoTen.Clear();
                            txtDiaChi.Clear();
                            txtSDT.Clear();

                            this.Close();
                        }
                        else MessageBox.Show("Có lỗi xảy ra !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        MessageBox.Show("Kiểm Tra Lại Giá Trị Ngày Sinh. Tuổi Nằm Trong Khoảng 12-65");
                }
                else
                {
                    MessageBox.Show("Kiểm Tra Lại Số Điện Thoại!");
                }
            }
            else
            {
                MessageBox.Show("Vui Lòng Kiểm Tra Lại Thông Tin");
            }

        }
    }
}
