﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using _1461589_1660215_1660218_1660259_1660290.DAO;
using _1461589_1660215_1660218_1660259_1660290.DTO;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fMember : Form
    {

        public fMember()
        {
            InitializeComponent();
        }

        private void fMember_Load(object sender, EventArgs e)
        {
            dataGridViewDanhSachThanhVien.DataSource = DAO.MemBerDAO.Instance.LoadDanhSachThanhVien();
            txtHoTenKH.Clear();
            txtDiaChiKH.Clear();
            txtSDTKH.Clear();
            txtTraCuuKH.Clear();

            dataGridViewDanhSachThanhVien_SelectionChanged(sender, e);
        }
        int idex = 0;
        string ID;

        private void dataGridViewDanhSachThanhVien_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridViewDanhSachThanhVien.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                idex = dataGridViewDanhSachThanhVien.CurrentRow.Index;
                ID = dataGridViewDanhSachThanhVien.Rows[idex].Cells[0].Value.ToString();
                txtHoTenKH.Text = dataGridViewDanhSachThanhVien.Rows[idex].Cells[1].Value.ToString();
                string m = dataGridViewDanhSachThanhVien.Rows[idex].Cells[2].Value.ToString();
                if (m == "True")
                {
                    cbxGioiTinhKH.Text = @"Nam";
                }
                if (m == "False")
                {
                    cbxGioiTinhKH.Text = @"Nữ";
                }
                txtDiaChiKH.Text = dataGridViewDanhSachThanhVien.Rows[idex].Cells[3].Value.ToString();
                txtSDTKH.Text = dataGridViewDanhSachThanhVien.Rows[idex].Cells[4].Value.ToString();
                dateTimePickerNgayDangKi.Text = dataGridViewDanhSachThanhVien.Rows[idex].Cells[6].Value.ToString();
                dateTimePickerNgayKetThuc.Text = dataGridViewDanhSachThanhVien.Rows[idex].Cells[7].Value.ToString();
                txtGoiTap.Text = dataGridViewDanhSachThanhVien.Rows[idex].Cells[8].Value.ToString();
                txtHLV.Text = dataGridViewDanhSachThanhVien.Rows[idex].Cells[9].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Không thể click chọn ở đây!", "Ui, có gì đó không đúng!", MessageBoxButtons.OK);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            dataGridViewDanhSachThanhVien.DataSource = MemBerDAO.Instance.LayDanhSachTaiKhoanTuSDT(txtTraCuuKH.Text);
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnLuuCS_Click(object sender, EventArgs e)
        {
            if (txtHoTenKH.Text != "" && txtDiaChiKH.Text != "" && txtSDTKH.Text != "")
            {
                if (IsNumber(txtSDTKH.Text))
                {
                    int cmbGioiTinhKH1;
                    if (cbxGioiTinhKH.Text == @"Nam")
                    {
                        cmbGioiTinhKH1 = 1;
                        if (DAO.MemBerDAO.Instance.SuaThongTinThanhVien(txtHoTenKH.Text, cmbGioiTinhKH1, txtDiaChiKH.Text, txtSDTKH.Text, ID))
                        {
                            MessageBox.Show("Sửa Thành Công!");
                            dataGridViewDanhSachThanhVien.DataSource = DAO.MemBerDAO.Instance.LoadDanhSachThanhVien();
                            txtHoTenKH.Clear();
                            txtDiaChiKH.Clear();
                            txtSDTKH.Clear();
                            txtTraCuuKH.Clear();
                        }
                        else
                        {
                            MessageBox.Show("Kiểm Tra Lại Thông Tin!");
                        }
                    }
                    if (cbxGioiTinhKH.Text == @"Nữ")
                    {
                        cmbGioiTinhKH1 = 0;
                        if (DAO.MemBerDAO.Instance.SuaThongTinThanhVien(txtHoTenKH.Text, cmbGioiTinhKH1, txtDiaChiKH.Text, txtSDTKH.Text, ID))
                        {
                            MessageBox.Show("Sửa Thành Công!");
                            txtHoTenKH.Clear();
                            txtDiaChiKH.Clear();
                            txtSDTKH.Clear();
                            txtTraCuuKH.Clear();
                            dataGridViewDanhSachThanhVien.DataSource = DAO.MemBerDAO.Instance.LoadDanhSachThanhVien();
                        }
                        else
                        {
                            MessageBox.Show("Kiểm Tra Lại Thông Tin!");
                        }
                    }

                }
                else
                    MessageBox.Show("Kiểm Tra Lại Số Điện Thoại!");
            }
            else
            {
                MessageBox.Show("Cần Kiểm Tra Và Nhập Đầy Đủ Thông Tin");
            }

        }

        private void btnGiaHan_Click(object sender, EventArgs e)
        {
            fGiaHanGoiTap f = new fGiaHanGoiTap();
            f.IDMB = ID;
            f.HoTenMB = dataGridViewDanhSachThanhVien.Rows[idex].Cells[1].Value.ToString();
            f.DiaChiMB = dataGridViewDanhSachThanhVien.Rows[idex].Cells[3].Value.ToString();
            f.SDTMB = dataGridViewDanhSachThanhVien.Rows[idex].Cells[4].Value.ToString();
            f.NgayDKMB = dataGridViewDanhSachThanhVien.Rows[idex].Cells[6].Value.ToString();
            f.NgayKTMB = dataGridViewDanhSachThanhVien.Rows[idex].Cells[7].Value.ToString();
            f.GoiTap = dataGridViewDanhSachThanhVien.Rows[idex].Cells[8].Value.ToString();
            f.HLV  = dataGridViewDanhSachThanhVien.Rows[idex].Cells[9].Value.ToString();
            f.GioiTinhMB = cbxGioiTinhKH.Text;
            f.ShowDialog();
            fMember_Load(sender, e);
        }

        
    }
}
