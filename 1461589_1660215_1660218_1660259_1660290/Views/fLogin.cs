﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using _1461589_1660215_1660218_1660259_1660290.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290
{
    public partial class fLogin : Form
    {
        public fLogin()
        {
            InitializeComponent();
        }

        bool Login(string userName, string passWord)
        {

            return TaiKhoanDAO.Instance.Login(userName,passWord);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void fLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có thực sự muốn thoát ứng dụng?", "Thoát ứng dụng", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.OK)
            {
                e.Cancel = true;
            }
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            /*
               //Tạo MD5 
               MD5 mh = MD5.Create();
               //Chuyển kiểu chuổi thành kiểu byte
               byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(txtPassword.Text);
               //mã hóa chuỗi đã chuyển
               byte[] hash = mh.ComputeHash(inputBytes);
               //tạo đối tượng StringBuilder (làm việc với kiểu dữ liệu lớn)
               StringBuilder sb = new StringBuilder();

               for (int i = 0; i < hash.Length; i++)
               {
                   sb.Append(hash[i].ToString("x2"));
               }
               MessageBox.Show(sb.ToString());
               //e10adc3949ba59abbe56e057f20f883e   -- 123456
               //nếu bạn muốn các chữ cái in thường thay vì in hoa thì bạn thay chữ "X" in hoa trong "X2" thành "x"
               */


            if (Login(txtUserName.Text, txtPassword.Text))
            {
                TaiKhoanDTO login = TaiKhoanDAO.Instance.GetAccountByUserName(txtUserName.Text);
                if (login.TinhTrang)
                {
                    this.Hide();
                    fManage f = new fManage(login);
                    f.ShowDialog();
                    this.Show();
                }
                else
                {
                    MessageBox.Show("Tài khoản của bạn bị khóa, vui lòng liên hệ chủ phòng tập!", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Tài khoản hoặc mật khẩu không chính xác !", "Ui, có gì đó không ổn?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void fLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
