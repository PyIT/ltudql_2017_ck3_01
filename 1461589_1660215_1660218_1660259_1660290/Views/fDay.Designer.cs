﻿namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    partial class fDay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbCustomInfor = new System.Windows.Forms.GroupBox();
            this.cb_BM = new System.Windows.Forms.ComboBox();
            this.datetime_picker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_SDT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_Ten = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblForm = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_TT = new System.Windows.Forms.TextBox();
            this.btnInHoaDon = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.grbCustomInfor.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbCustomInfor
            // 
            this.grbCustomInfor.Controls.Add(this.cb_BM);
            this.grbCustomInfor.Controls.Add(this.datetime_picker);
            this.grbCustomInfor.Controls.Add(this.label4);
            this.grbCustomInfor.Controls.Add(this.label3);
            this.grbCustomInfor.Controls.Add(this.txt_SDT);
            this.grbCustomInfor.Controls.Add(this.label2);
            this.grbCustomInfor.Controls.Add(this.txt_Ten);
            this.grbCustomInfor.Controls.Add(this.label1);
            this.grbCustomInfor.Location = new System.Drawing.Point(40, 68);
            this.grbCustomInfor.Name = "grbCustomInfor";
            this.grbCustomInfor.Size = new System.Drawing.Size(627, 124);
            this.grbCustomInfor.TabIndex = 0;
            this.grbCustomInfor.TabStop = false;
            this.grbCustomInfor.Text = "Thông tin khách";
            // 
            // cb_BM
            // 
            this.cb_BM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_BM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_BM.FormattingEnabled = true;
            this.cb_BM.Location = new System.Drawing.Point(451, 72);
            this.cb_BM.Name = "cb_BM";
            this.cb_BM.Size = new System.Drawing.Size(149, 28);
            this.cb_BM.TabIndex = 7;
            this.cb_BM.SelectedIndexChanged += new System.EventHandler(this.cb_BM_SelectedIndexChanged);
            // 
            // datetime_picker
            // 
            this.datetime_picker.Enabled = false;
            this.datetime_picker.Location = new System.Drawing.Point(87, 74);
            this.datetime_picker.Name = "datetime_picker";
            this.datetime_picker.Size = new System.Drawing.Size(243, 24);
            this.datetime_picker.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(368, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Bộ Môn:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngày:";
            // 
            // txt_SDT
            // 
            this.txt_SDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SDT.Location = new System.Drawing.Point(451, 32);
            this.txt_SDT.Name = "txt_SDT";
            this.txt_SDT.Size = new System.Drawing.Size(149, 26);
            this.txt_SDT.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(400, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "SĐT:";
            // 
            // txt_Ten
            // 
            this.txt_Ten.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Ten.Location = new System.Drawing.Point(87, 31);
            this.txt_Ten.Name = "txt_Ten";
            this.txt_Ten.Size = new System.Drawing.Size(243, 26);
            this.txt_Ten.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Họ Tên:";
            // 
            // lblForm
            // 
            this.lblForm.AutoSize = true;
            this.lblForm.Font = new System.Drawing.Font("Times New Roman", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForm.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblForm.Location = new System.Drawing.Point(218, 18);
            this.lblForm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForm.Name = "lblForm";
            this.lblForm.Size = new System.Drawing.Size(299, 35);
            this.lblForm.TabIndex = 2;
            this.lblForm.Text = "Đăng ký tập theo ngày";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Enabled = false;
            this.label8.Location = new System.Drawing.Point(419, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 18);
            this.label8.TabIndex = 29;
            this.label8.Text = "VNĐ";
            // 
            // txt_TT
            // 
            this.txt_TT.Enabled = false;
            this.txt_TT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TT.Location = new System.Drawing.Point(208, 218);
            this.txt_TT.Name = "txt_TT";
            this.txt_TT.Size = new System.Drawing.Size(205, 30);
            this.txt_TT.TabIndex = 31;
            // 
            // btnInHoaDon
            // 
            this.btnInHoaDon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInHoaDon.Location = new System.Drawing.Point(491, 204);
            this.btnInHoaDon.Name = "btnInHoaDon";
            this.btnInHoaDon.Size = new System.Drawing.Size(176, 61);
            this.btnInHoaDon.TabIndex = 32;
            this.btnInHoaDon.Text = "In Hóa Đơn";
            this.btnInHoaDon.UseVisualStyleBackColor = true;
            this.btnInHoaDon.Click += new System.EventHandler(this.btnInHoaDon_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(102, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 25);
            this.label7.TabIndex = 30;
            this.label7.Text = "Tổng tiền:";
            // 
            // fDay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 277);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_TT);
            this.Controls.Add(this.btnInHoaDon);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblForm);
            this.Controls.Add(this.grbCustomInfor);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "fDay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Đăng ký tập theo ngày";
            this.Load += new System.EventHandler(this.fDay_Load);
            this.grbCustomInfor.ResumeLayout(false);
            this.grbCustomInfor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbCustomInfor;
        private System.Windows.Forms.Label lblForm;
        private System.Windows.Forms.ComboBox cb_BM;
        private System.Windows.Forms.DateTimePicker datetime_picker;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_SDT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_Ten;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_TT;
        private System.Windows.Forms.Button btnInHoaDon;
        private System.Windows.Forms.Label label7;
    }
}