﻿namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    partial class fAdminQLP_QuanLiLichHLV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnSua = new System.Windows.Forms.Button();
            this.grbGoiTap = new System.Windows.Forms.GroupBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblFullName = new System.Windows.Forms.Label();
            this.grbMemberList = new System.Windows.Forms.GroupBox();
            this.dataGridViewDSCongViecHLV = new System.Windows.Forms.DataGridView();
            this.lblForm = new System.Windows.Forms.Label();
            this.grbGoiTap.SuspendLayout();
            this.grbMemberList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDSCongViecHLV)).BeginInit();
            this.SuspendLayout();
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Location = new System.Drawing.Point(429, 334);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(134, 42);
            this.btnXoa.TabIndex = 29;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnSua
            // 
            this.btnSua.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua.Location = new System.Drawing.Point(429, 263);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(134, 42);
            this.btnSua.TabIndex = 28;
            this.btnSua.Text = "Sửa";
            this.btnSua.UseVisualStyleBackColor = true;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // grbGoiTap
            // 
            this.grbGoiTap.Controls.Add(this.dateTimePicker2);
            this.grbGoiTap.Controls.Add(this.dateTimePicker1);
            this.grbGoiTap.Controls.Add(this.label1);
            this.grbGoiTap.Controls.Add(this.label2);
            this.grbGoiTap.Controls.Add(this.textBox2);
            this.grbGoiTap.Controls.Add(this.lblFullName);
            this.grbGoiTap.Location = new System.Drawing.Point(6, 245);
            this.grbGoiTap.Name = "grbGoiTap";
            this.grbGoiTap.Size = new System.Drawing.Size(372, 149);
            this.grbGoiTap.TabIndex = 26;
            this.grbGoiTap.TabStop = false;
            this.grbGoiTap.Text = "Thông tin chi tiết";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(89, 103);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(266, 26);
            this.dateTimePicker2.TabIndex = 13;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(89, 65);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(266, 26);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Ngày KT:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Ngày BĐ:";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(89, 26);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(266, 26);
            this.textBox2.TabIndex = 7;
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = true;
            this.lblFullName.Location = new System.Drawing.Point(43, 29);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(40, 20);
            this.lblFullName.TabIndex = 0;
            this.lblFullName.Text = "Tên:";
            // 
            // grbMemberList
            // 
            this.grbMemberList.Controls.Add(this.dataGridViewDSCongViecHLV);
            this.grbMemberList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbMemberList.Location = new System.Drawing.Point(6, 41);
            this.grbMemberList.Name = "grbMemberList";
            this.grbMemberList.Size = new System.Drawing.Size(596, 198);
            this.grbMemberList.TabIndex = 25;
            this.grbMemberList.TabStop = false;
            this.grbMemberList.Text = "Danh sách";
            // 
            // dataGridViewDSCongViecHLV
            // 
            this.dataGridViewDSCongViecHLV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDSCongViecHLV.Location = new System.Drawing.Point(6, 23);
            this.dataGridViewDSCongViecHLV.Name = "dataGridViewDSCongViecHLV";
            this.dataGridViewDSCongViecHLV.Size = new System.Drawing.Size(582, 169);
            this.dataGridViewDSCongViecHLV.TabIndex = 0;
            this.dataGridViewDSCongViecHLV.SelectionChanged += new System.EventHandler(this.dataGridViewDSCongViecHLV_SelectionChanged);
            // 
            // lblForm
            // 
            this.lblForm.AutoSize = true;
            this.lblForm.Font = new System.Drawing.Font("Times New Roman", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForm.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblForm.Location = new System.Drawing.Point(171, 9);
            this.lblForm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForm.Name = "lblForm";
            this.lblForm.Size = new System.Drawing.Size(252, 35);
            this.lblForm.TabIndex = 24;
            this.lblForm.Text = "Lịch làm việc HLV";
            // 
            // fAdminQLP_QuanLiLichHLV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 407);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.grbGoiTap);
            this.Controls.Add(this.grbMemberList);
            this.Controls.Add(this.lblForm);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "fAdminQLP_QuanLiLichHLV";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lí lịch HLV";
            this.Load += new System.EventHandler(this.fAdminQLP_QuanLiLichHLV_Load);
            this.grbGoiTap.ResumeLayout(false);
            this.grbGoiTap.PerformLayout();
            this.grbMemberList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDSCongViecHLV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.GroupBox grbGoiTap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label lblFullName;
        private System.Windows.Forms.GroupBox grbMemberList;
        private System.Windows.Forms.DataGridView dataGridViewDSCongViecHLV;
        private System.Windows.Forms.Label lblForm;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}