﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fAdminThemHLV : Form
    {
        public fAdminThemHLV()
        {
            InitializeComponent();
        }

        private void fAdminThemHLV_Load(object sender, EventArgs e)
        {
            // Get DS Bo Mon tu DB, Set vaule control Bo Mon
            cbBoMon.DataSource = BoMonDAO.Instance.LayDSBoMon();
            cbBoMon.DisplayMember = "TenBM";

            // Set control Gioi Tinh
            cbGT.Items.Clear();
            cbGT.Items.Add("Nữ");
            cbGT.Items.Add("Nam");
            cbGT.Text = "Nam";
        }

        private void btnChup_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtHoTen.Text != "" & txtDiaChi.Text != "" && txtEmail.Text != "" && txtSDT.Text != "" && txtLuong.Text != "" && cbGT.Text != "" && txtBangCap.Text != "" && cbBoMon.Text != "")
            {
                if (IsNumber(txtSDT.Text))
                {
                    if (IsNumber(txtLuong.Text))
                    {

                        // Xu ly ngay sinh
                        // Get year, month, day
                        string[] resultGetDate = dateTimePickerNgaySinh.Value.ToString().Split(new char[] { '/' });
                        int month = Convert.ToInt32(resultGetDate[0]);
                        int day = Convert.ToInt32(resultGetDate[1]);
                        string[] rsYear = resultGetDate[2].Split(new char[] { ' ' });
                        int year = Convert.ToInt32(rsYear[0]);
                        // Get DATE
                        string NgaySinh = year.ToString() + '-' + month.ToString() + '-' + day.ToString();

                        //Xu li gioi tinh
                        // Get sex
                        int sex = 0;
                        if (cbGT.Text == "Nam")
                            sex = 1;

                        if (HLVCaNhanDAO.Instance.ThemHLV(txtHoTen.Text, sex, txtLuong.Text, txtBangCap.Text, txtDiaChi.Text, txtEmail.Text, txtSDT.Text, cbBoMon.Text, NgaySinh))
                        {
                            MessageBox.Show("Thêm tài khoản HLV thành công ", "Thành công", MessageBoxButtons.OK);
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Lương chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Số điện thoại chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Cần nhập đầy đủ thông tin", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
