﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fAdminQLStore : Form
    {
        public fAdminQLStore()
        {
            InitializeComponent();
        }

        private void fAdminQLStore_Load(object sender, EventArgs e)
        {
            dataGridViewDSSP.DataSource = SanPhamDAO.Instance.LayDSSP();
        }

        private void dataGridViewDSSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int index = dataGridViewDSSP.CurrentRow.Index;

                txtMa.Text = dataGridViewDSSP.Rows[index].Cells[0].Value.ToString();
                txtTen.Text = dataGridViewDSSP.Rows[index].Cells[1].Value.ToString();
                txtGia.Text = dataGridViewDSSP.Rows[index].Cells[2].Value.ToString();
                txtSL.Text = dataGridViewDSSP.Rows[index].Cells[3].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Không thể click chọn ở đây!", "Ui, có gì đó không đúng!", MessageBoxButtons.OK);
            }
        }
        // Check string all num
        bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if(txtMa.Text == "")
            {
                if(txtTen.Text != "" && txtGia.Text != "" && txtSL.Text != "")
                {
                    if (IsNumber(txtGia.Text)) {
                        if (IsNumber(txtSL.Text))
                        {
                            if (SanPhamDAO.Instance.ThemSP(txtTen.Text, txtGia.Text, txtSL.Text))
                            {
                                MessageBox.Show("Thêm sản phẩm thành công", "Thành công!", MessageBoxButtons.OK);
                                fAdminQLStore_Load(sender, e);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Số lượng chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Giá chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Không được để trống Tên sản phẩm, Giá tiền hoặc Số lượng", "UI, có gì đó không đúng!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Mã sản phẩm đã tồn tại, vui lòng click vào dòng trống trên danh sách sản phẩm để xóa thông tin mã sản phẩm", "UI, có gì đó không đúng!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMa.Text != "" && txtTen.Text != "" && txtGia.Text != "" && txtSL.Text != "")
            {
                if (IsNumber(txtGia.Text))
                {
                    if (IsNumber(txtSL.Text))
                    {
                        if (SanPhamDAO.Instance.XoaSP(txtMa.Text))
                        {
                            MessageBox.Show("Xóa sản phẩm thành công", "Thành công!", MessageBoxButtons.OK);
                            fAdminQLStore_Load(sender, e);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Số lượng chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Giá chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Không được để trống thông tin", "UI, có gì đó không đúng!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (txtMa.Text != "" && txtTen.Text != "" && txtGia.Text != "" && txtSL.Text != "")
            {
                if (IsNumber(txtGia.Text))
                {
                    if (IsNumber(txtSL.Text))
                    {
                        if (SanPhamDAO.Instance.SuaSP(txtMa.Text,txtTen.Text, txtGia.Text, txtSL.Text))
                        {
                            MessageBox.Show("Sửa sản phẩm thành công", "Thành công!", MessageBoxButtons.OK);
                            fAdminQLStore_Load(sender, e);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Số lượng chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Giá chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Không được để trống thông tin", "UI, có gì đó không đúng!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
