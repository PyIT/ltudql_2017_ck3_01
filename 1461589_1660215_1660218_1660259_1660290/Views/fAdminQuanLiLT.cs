﻿using _1461589_1660215_1660218_1660259_1660290.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1461589_1660215_1660218_1660259_1660290.Views
{
    public partial class fAdminQuanLiLT : Form
    {
        public fAdminQuanLiLT()
        {
            InitializeComponent();
        }

        private void btnThemLT_Click(object sender, EventArgs e)
        {
            //this.Hide();
            fAdminThemLT f = new fAdminThemLT();
            f.ShowDialog();
            //this.Show();
            fAdminQuanLiLT_Load(sender, e);
        }

        private void fAdminQuanLiLT_Load(object sender, EventArgs e)
        {
            dataGridViewDSLT.DataSource = TaiKhoanDAO.Instance.LayDSLeTan();

            cbGT.Items.Clear();
            cbGT.Items.Add("Nữ");
            cbGT.Items.Add("Nam");
            cbGT.Text = "Nam";

            cbTT.Items.Clear();
            cbTT.Items.Add("Active");
            cbTT.Items.Add("Block");
            cbTT.Text = "Block";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dataGridViewDSLT.DataSource = TaiKhoanDAO.Instance.LayDSLTTuSDT(txtSearchSDT.Text);
        }

        private void dataGridViewDSLT_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int index = dataGridViewDSLT.CurrentRow.Index;

                txtTenDangNhap.Text = dataGridViewDSLT.Rows[index].Cells[0].Value.ToString();
                txtMatKhau.Text = dataGridViewDSLT.Rows[index].Cells[1].Value.ToString();
                txtHoTen.Text = dataGridViewDSLT.Rows[index].Cells[2].Value.ToString();

                //Set control GioiTinh
                if (dataGridViewDSLT.Rows[index].Cells[3].Value.ToString() == "True")
                    cbGT.Text = @"Nam";
                else cbGT.Text = @"Nữ";
                //Set control Trang thai
                if (dataGridViewDSLT.Rows[index].Cells[9].Value.ToString() == "True")
                    cbTT.Text = @"Active";
                else cbTT.Text = @"Block";

                txtBC.Text = dataGridViewDSLT.Rows[index].Cells[4].Value.ToString();
                txtSDT.Text = dataGridViewDSLT.Rows[index].Cells[5].Value.ToString();
                txtEmail.Text = dataGridViewDSLT.Rows[index].Cells[6].Value.ToString();
                txtDiaChi.Text = dataGridViewDSLT.Rows[index].Cells[7].Value.ToString();
                txtLuong.Text = dataGridViewDSLT.Rows[index].Cells[8].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Không thể click chọn ở đây!", "Ui, có gì đó không đúng!", MessageBoxButtons.OK);
            }
        }

        private void btnChup_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnLuuAnh_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang được xây dựng", "Xin lỗi bạn!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        // Check string all num
        bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (txtTenDangNhap.Text != "" && txtHoTen.Text != "" && txtSDT.Text != "" && txtDiaChi.Text != "" && txtEmail.Text != "" && txtLuong.Text != "" && txtBC.Text != "" && cbGT.Text != "" && cbTT.Text != "")
            {
                if (IsNumber(txtSDT.Text))
                {
                    if (IsNumber(txtLuong.Text))
                    {
                        int GT = 0;
                        if (cbGT.Text == "Nam") GT = 1;

                        int TT = 0;
                        if (cbTT.Text == "Active") TT = 1;

                        if(TaiKhoanDAO.Instance.SuaThongTinLT(txtTenDangNhap.Text, txtHoTen.Text, GT, txtLuong.Text, txtBC.Text, txtDiaChi.Text, txtEmail.Text, txtSDT.Text, TT))
                        {
                            MessageBox.Show("Lưu chỉnh sửa thành công !", "Thành công", MessageBoxButtons.OK);
                            fAdminQuanLiLT_Load(sender, e);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Lương chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Số điện thoại chỉ được chứa số", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Cần nhập đầy đủ thông tin", "Có gì đó hông đúng?", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
