﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1461589_1660215_1660218_1660259_1660290.DTO
{
    class TaiKhoanDTO
    {
        public TaiKhoanDTO(string maTK, string tenDangNhap, bool loaiTK, bool gioiTinh, string hoTen, string email, string sDT, string diaChi, string luongCB, string ngaySinh, bool tinhTrang, string anhTK, string matKhau = null)
        {
            this.MaTK = maTK;
            this.TenDangNhap = tenDangNhap;
            this.LoaiTK = loaiTK;
            this.GioiTinh = gioiTinh;
            this.HoTen = hoTen;
            this.Email = email;
            this.SDT = sDT;
            this.DiaChi = diaChi;
            this.LuongCB = luongCB;
            this.NgaySinh = ngaySinh;
            this.TinhTrang = tinhTrang;
            this.AnhTK = anhTK;
            this.MatKhau = matKhau;
        }
        public TaiKhoanDTO(DataRow row)
        {
            this.MaTK = row["MaTK"].ToString();
            this.TenDangNhap = row["TenDangNhap"].ToString();
            this.LoaiTK = (bool)row["LoaiTK"];
            this.GioiTinh = (bool)row["GioiTinh"];
            this.HoTen = row["HoTen"].ToString();
            this.Email = row["email"].ToString();
            this.SDT = row["SDT"].ToString();
            this.DiaChi = row["DiaChi"].ToString();
            this.LuongCB = row["LuongCB"].ToString();
            this.NgaySinh = row["NgaySinh"].ToString();
            this.TinhTrang = (bool)row["TinhTrang"];
            this.AnhTK = row["AnhTK"].ToString();
            this.MatKhau = row["MatKhau"].ToString();
        }
        public TaiKhoanDTO() { }

        private string maTK;
        private string tenDangNhap;
        private string matKhau;
        private bool loaiTK;
        private bool gioiTinh;
        private string hoTen;
        private string email;
        private string sDT;
        private string diaChi;
        private string luongCB;
        private string ngaySinh;
        private bool tinhTrang;
        private string anhTK;


        public string MaTK { get => maTK; set => maTK = value; }
        public string TenDangNhap { get => tenDangNhap; set => tenDangNhap = value; }
        public string MatKhau { get => matKhau; set => matKhau = value; }
        public bool LoaiTK { get => loaiTK; set => loaiTK = value; }
        public bool GioiTinh { get => gioiTinh; set => gioiTinh = value; }
        public string HoTen { get => hoTen; set => hoTen = value; }
        public string Email { get => email; set => email = value; }
        public string SDT { get => sDT; set => sDT = value; }
        public string DiaChi { get => diaChi; set => diaChi = value; }
        public string LuongCB { get => luongCB; set => luongCB = value; }
        public string NgaySinh { get => ngaySinh; set => ngaySinh = value; }
        public bool TinhTrang { get => tinhTrang; set => tinhTrang = value; }
        public string AnhTK { get => anhTK; set => anhTK = value; }
    }

}
