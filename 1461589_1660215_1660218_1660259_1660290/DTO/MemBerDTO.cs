﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1461589_1660215_1660218_1660259_1660290.DTO
{
    class MemBerDTO
    {
        private int maKH;
        private string hoTenKH;
        private bool gioiTinhKH;
        private string diaChiKH;
        private string sdtKH;
        private string ngaySinhKH;
        private string ngayDangKyKH;
        private string ngayKetThucKH;
        private string anhKH;
        private string goiTap;
        private int maPT;
        private int maHD;

        public int MaKH { get => maKH; set => maKH = value; }
        public string HoTenKH { get => hoTenKH; set => hoTenKH = value; }
        public bool GioiTinhKH { get => gioiTinhKH; set => gioiTinhKH = value; }
        public string DiaChiKH { get => diaChiKH; set => diaChiKH = value; }
        public string SdtKH { get => sdtKH; set => sdtKH = value; }
        public string NgaySinhKH { get => ngaySinhKH; set => ngaySinhKH = value; }
        public string NgayDangKyKH { get => ngayDangKyKH; set => ngayDangKyKH = value; }
        public string NgayKetThucKH { get => ngayKetThucKH; set => ngayKetThucKH = value; }
        public string AnhKH { get => anhKH; set => anhKH = value; }
        public string GoiTap { get => goiTap; set => goiTap = value; }
        public int MaPT { get => maPT; set => maPT = value; }
        public int MaHD { get => maHD; set => maHD = value; }

        public MemBerDTO(int maKH, string hoTenKH, bool gioiTinhKH, string diaChiKH, string sdtKh, string ngaySinhKH, string ngayDangKyKH, string ngayKetThucKH, string anhKH, string goiTap, int maPT, int maHD)
        {
            this.MaKH = maKH;
            this.HoTenKH = hoTenKH;
            this.GioiTinhKH = gioiTinhKH;
            this.DiaChiKH = diaChiKH;
            this.SdtKH = sdtKh;
            this.NgaySinhKH = ngaySinhKH;
            this.NgayDangKyKH = ngayDangKyKH;
            this.NgayKetThucKH = ngayKetThucKH;
            this.AnhKH = anhKH;
            this.GoiTap = goiTap;
            this.MaPT = maPT;
            this.MaHD = maHD;
        }
        public MemBerDTO(DataRow row)
        {
            this.MaKH = (int)row["MaKH"];
            this.HoTenKH = row["HoTenKH"].ToString();
            this.GioiTinhKH = (bool)row["GioiTinhKH"];
            this.DiaChiKH = row["DiaChiKH"].ToString();
            this.SdtKH = row["SdtKH"].ToString();
            this.NgaySinhKH = row["NgaySinhKH"].ToString();
            this.NgayDangKyKH = row["NgayDangKyKH"].ToString();
            this.NgayKetThucKH = row["NgayKetThucKH"].ToString();
            this.AnhKH = row["AnhKH"].ToString(); ;
            this.GoiTap = row["GoiTap"].ToString();
            this.MaPT = (int)row["MaPT"];
            this.MaHD = (int)row["MaHD"];
        }
        public MemBerDTO() { }

    }

}
