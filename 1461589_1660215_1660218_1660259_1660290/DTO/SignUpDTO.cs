﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace _1461589_1660215_1660218_1660259_1660290.DTO
{
    class SignUpDTO
    {
        private int maKH;
        private string hoTenKH;
        private int gioiTinhKH;
        private string diaChiKH;
        private string sdtKH;
        private string ngaySinhKH;
        private string ngayDangKiKH;
        private string ngayKetThucKH;
        private string anhKH;
        private string gioiTap;
        private int maPT;
        private int maHD;

        public int MaKH { get => maKH; set => maKH = value; }
        public string HoTenKH { get => hoTenKH; set => hoTenKH = value; }
        public int GioiTinhKH { get => gioiTinhKH; set => gioiTinhKH = value; }
        public string DiaChiKH { get => diaChiKH; set => diaChiKH = value; }
        public string SdtKH { get => sdtKH; set => sdtKH = value; }
        public string NgaySinhKH { get => ngaySinhKH; set => ngaySinhKH = value; }
        public string NgayDangKiKH { get => ngayDangKiKH; set => ngayDangKiKH = value; }
        public string NgayKetThucKH { get => ngayKetThucKH; set => ngayKetThucKH = value; }
        public string AnhKH { get => anhKH; set => anhKH = value; }
        public string GioiTap { get => gioiTap; set => gioiTap = value; }
        public int MaPT { get => maPT; set => maPT = value; }
        public int MaHD { get => maHD; set => maHD = value; }

        public SignUpDTO(int maKH, string hoTenKH, int gioiTinhKH, string diaChiKH, string sdtKh, string ngaySinhKH, string ngayDangKiKH, string ngayKetThucKH, string anhKH, string goiTap, int maPT, int maHD)
        {
            this.MaKH = maKH;
            this.HoTenKH = hoTenKH;
            this.GioiTinhKH = gioiTinhKH;
            this.DiaChiKH = diaChiKH;
            this.SdtKH = sdtKh;
            this.NgaySinhKH = ngaySinhKH;
            this.NgayDangKiKH = ngayDangKiKH;
            this.NgayKetThucKH = ngayKetThucKH;
            this.AnhKH = anhKH;
            this.GioiTap = goiTap;
            this.MaPT = maPT;
            this.MaHD = maHD;
        }
        public SignUpDTO(DataRow row)
        {
            this.MaKH = (int)row["MaKH"];
            this.HoTenKH = row["HoTenKH"].ToString();
            this.GioiTinhKH = (int)row["GioiTinhKH"];
            this.DiaChiKH = row["DiaChiKH"].ToString();
            this.SdtKH = row["SdtKH"].ToString();
            this.NgaySinhKH = row["NgaySinhKH"].ToString();
            this.NgayDangKiKH = row["NgayDangKiKH"].ToString();
            this.NgayKetThucKH = row["NgayKetThucKH"].ToString();
            this.AnhKH = row["AnhKH"].ToString(); ;
            this.GioiTap = row["GioiTap"].ToString();
            this.MaPT = (int)row["MaPT"];
            this.MaHD = (int)row["MaHD"];
        }
        public SignUpDTO() { }

    }
}
